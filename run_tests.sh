#!/bin/bash

set -o pipefail

DIR=build
if [ $# -eq 1 ]
then
	DIR=$1
fi

ls ${DIR}/bin/test* |
while read line
do
	echo "             running $line :"
	./$line 2>&1 | grep "TestSuite: " \
	| sed "s/^.*TestSuite/TestSuite/" \
	;
	EXIT_CODE=$?
	if [ $EXIT_CODE -ne 0 ]
	then
		exit $EXIT_CODE
	fi
done

ANY_ERROR=$?
if [ $ANY_ERROR -ne 0 ]
then
	echo Tests errored!
else
	echo Tests done
fi
