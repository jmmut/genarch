#!/bin/bash

if [ ! -d build ]
then
	mkdir build
	cd build
	conan install .. --build missing
	cmake -G Ninja -DCMAKE_BUILD_TYPE=Debug ..
	cd ..
fi

cd build
ninja && cd .. && ./run_tests.sh
exit $?
