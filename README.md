# Genarch

This project is an experiment about programs that can modify themselves to solve problems.

The idea is for a program to understand that it can not solve a given problem, but know some heuristics on how to modify its current strategy to be successful.

For this, a simple high-level virtual machine is available. The instruction set has high level commands such as "copy a whole program", "modify instruction", "send data to problem", and such.

## Requirements to build the program

- C++ compiler (`sudo apt-get install build-essential`)
- opengl (`sudo apt-get install libgl1-mesa-dev`)
- cmake (`sudo apt-get install cmake`)
- conan (download [here](https://www.conan.io/downloads), recommended:
  `sudo apt-get install python-pip; sudo pip install setuptools wheel conan`)


## Compiling

Check that you have the requirements installed, and do the following:

Linux:

```
git clone https://bitbucket.org/jmmut/genarch.git
cd genarch
mkdir build && cd build
conan install --build missing ..
cmake -G "Unix Makefiles" ..
make
./bin/genarch
```

Windows:

```
git clone https://bitbucket.org/jmmut/genarch.git
cd genarch
conan install --build missing
cmake -G "Visual Studio 14 Win64"
cmake --build . --config Release
./bin/genarch.exe
```
## Examples

The first program and problem are obviously very basic and limited, but they are meant as the hello world; the simplest example as a proof of concept.

Given a problem "GiveMe100Twice" where the solution is "[100, 100]" (this is a vector of 2 elements with value 100), the next program finds the solution eventually.

```
WHAT_IS_MY_REFERENCE(0) [5]
GET_RANDOM(12) [1, -1000, 1000]
COPY_PROGRAM(1) [6, 5]
CHANGE_VALUE_OF_INSTRUCTION_OF_PROGRAM(9) [1, 6, 6, 2, 1]
REQUEST_INPUT_SIZE(2) [1]
STORE_N_COPIES(6) [1, 100, 1]
MANIPULATE(3) [1]
PERCEIVE(4) [1]
```

This is a human readable representation of the program. During execution, each instruction is stored in a register, which is a list of integer values, and the list of instructions that make the program is stored in another register. The first value of an instruction is the code for each different instruction, represented here with a sentence and its code in parenthesis (e.g. GET_RANDOM(12)). The rest of the values are parameters. The second value is the address of the register where the return value will be stored, unless there's no return value such as in MANIPULATE(3). The other values are other input parameters and vary among instructions.

## Possible goals

```
[/] Be able to try different programming paradigms
  [x] On the same problem
  [/] Brute force (not general)
  [/] Simplified gradient descent (unidimensional)
  [ ] Bactracking
  [ ] Parameterized problems
[ ] Justify the genarch virtual machine instead of just functions
[ ] Genarch virtual machine features
  [x] Interact with problems
  [x] Call functions (subprograms)
  [x] Create functions on runtime
  [x] Modify instructions
  [/] "While" instruction (workaround: create subprograms callable by the engine)
  [ ] "If" instruction (workaround: execute both branches and kill the subprogram that fails)
```
