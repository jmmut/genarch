/**
 * @file basic.test.cpp
 */
#include <test/TestSuite.h>
#include <world/World.h>
#include <problems/SudokuRow.h>
#include <utils/SignalHandler.h>

using randomize::test::TestSuite;


std::pair<bool, std::vector<long>> subprogram(InputSize inputSize, long iteration, std::vector<long> recursionTracking,
        Problem &problem);

std::pair<bool, std::vector<long>> recursionSubprogram(InputSize inputSize, std::vector<long> recursionTracking,
        Problem &problem) {
    for (long i = 1; i <= inputSize ; ++i) {
        auto result = subprogram(inputSize, i, recursionTracking, problem);
        if (result.first) {
            return result;
        }
    }
    return {false, recursionTracking};
}


std::pair<bool, std::vector<long>> subprogram(InputSize inputSize, long iteration, std::vector<long> recursionTracking,
        Problem &problem) {
    recursionTracking.push_back(iteration);
    bool correctSize = problem.manipulate(recursionTracking);
    if (not correctSize) {
        return recursionSubprogram(inputSize, recursionTracking, problem);
    }
    return {problem.isSolved(), recursionTracking};
}


//std::pair<bool, std::vector<long>> recursion(InputSize inputSize) {
//    for (long i = 1; i <= inputSize; ++i) {
//
//    }
//}

int main(int argc, char **argv) {
    LOG_LEVEL(randomize::log::parseLogLevel(argc > 1? argv[1] : "INFO"));

    TestSuite{"backtracking: sudoku row", {
            []() {
                Reference whatIsMyReference = 5;
                Reference inputSize = 7;
                Reference loopResult = 10;
                Reference subprogram = 11;
                Reference recursionTracking = 12;
                Reference recursionSubprogramSpace = 13;
                Reference recursionWasExecutable = 14;
                Reference loopSubprogramReference = 15;
                Reference toplevelWasExecutable = 16;
                Reference loopWasExecutable = 17;
                Reference recursionWasExecutable2 = 18;
                std::vector<Register> instructions = concatenate(
                        makeSubprogram(subprogram, concatenate(
                                std::vector<Register>{
                                    makeSentence<Instruction::DELETE>({recursionSubprogramSpace}),
                                    makeSentence<Instruction::DELETE>({whatIsMyReference}),
                                    makeSentence<Instruction::DELETE>({loopWasExecutable}),
                                    makeSentence<Instruction::DELETE>({recursionWasExecutable}),
                                    makeSentence<Instruction::DELETE>({recursionWasExecutable2}),
                                },
                                makeSubprogram(recursionSubprogramSpace, std::vector<Register>{
                                        makeSentence<Instruction::LOOP>(
                                                {loopResult, loopSubprogramReference, inputSize}),
                                }),
                                std::vector<Register>{
                                        makeSentence<Instruction::CONCATENATE>(
                                                {recursionTracking, World::ITERATION_REGISTER}),
                                        makeSentence<Instruction::COPY_TO_SPACE>(
                                                {recursionTracking, recursionTracking, recursionSubprogramSpace}),
                                        makeSentence<Instruction::COPY_TO_SPACE>(
                                                {inputSize, inputSize, recursionSubprogramSpace}),
                                        makeSentence<Instruction::WHAT_IS_MY_REFERENCE>({whatIsMyReference}),
                                        makeSentence<Instruction::CANCEL_CALL_SUBPROGRAM_ASYNC>(
                                                {loopWasExecutable, whatIsMyReference}),
                                        makeSentence<Instruction::COPY_TO_SPACE>(
                                                {loopSubprogramReference, whatIsMyReference, recursionSubprogramSpace}),
                                        makeSentence<Instruction::CALL_SUBPROGRAM_ASYNC>(
                                                {recursionWasExecutable, recursionSubprogramSpace}),
                                        makeSentence<Instruction::MANIPULATE>({recursionTracking}),

                                        // if the manipulation didn't fail, the size was right: don't continue recursion
                                        makeSentence<Instruction::CANCEL_CALL_SUBPROGRAM_ASYNC>(
                                                {recursionWasExecutable2, recursionSubprogramSpace}),

                                }
                        )),
                        std::vector<Register>{
                                makeSentence<Instruction::DELETE>({whatIsMyReference}),
                                makeSentence<Instruction::DELETE>({inputSize}),
                                makeSentence<Instruction::DELETE>({loopResult}),
                                makeSentence<Instruction::WHAT_IS_MY_REFERENCE>({whatIsMyReference}),
                                makeSentence<Instruction::REQUEST_INPUT_SIZE>({inputSize}),
                                makeSentence<Instruction::COPY_TO_SPACE>(
                                        {inputSize, inputSize, subprogram}),
                                makeSentence<Instruction::LOOP>({loopResult, subprogram, inputSize}),
                                makeSentence<Instruction::CANCEL_CALL_SUBPROGRAM_ASYNC>(
                                        {toplevelWasExecutable, whatIsMyReference}),
                        }
                );


                World world{instructions, std::make_unique<SudokuRow>()};
                ASSERT_OR_THROW(not world.getProblem().isSolved());
                size_t maxAttempts = 10000;
//                return 0; // TODO deleteme
                for (size_t i = 0; i < maxAttempts; ++i) {
                    world.iterate();
                    if (world.getProblem().isSolved()) {
                        LOG_INFO("after %lu iterations, the problem was solved", i);
                        break;
                    }
                }
                ASSERT_OR_THROW_MSG(world.getProblem().isSolved(),
                        "after " + std::to_string(maxAttempts) + " attempts the problem should be solved");
            },
            []() {
                Reference manipulateWasExecutable = 3;
                Reference recursionTrackingLength = 4;
                Reference whatIsMyReference = 5;
                Reference inputSize = 7;
                Reference equals = 8;
                Reference notEquals = 9;
                Reference loopResult = 10;
                Reference subprogram = 11;
                Reference recursionTracking = 12;
                Reference recursionSubprogramSpace = 13;
                Reference recursionWasExecutable = 14;
                Reference loopSubprogramReference = 15;
                Reference toplevelWasExecutable = 16;
                Reference loopWasExecutable = 17;
                Reference recursionWasExecutable2 = 18;
                Reference manipulateSubprogramSpace = 19;

                std::vector<Register> instructions = concatenate(
                        makeSubprogram(subprogram, concatenate(
                                std::vector<Register>{
                                    makeSentence<Instruction::DELETE>({recursionSubprogramSpace}),
                                    makeSentence<Instruction::DELETE>({manipulateSubprogramSpace}),
                                    makeSentence<Instruction::DELETE>({whatIsMyReference}),
                                    makeSentence<Instruction::DELETE>({loopWasExecutable}),
                                    makeSentence<Instruction::DELETE>({recursionWasExecutable}),
                                    makeSentence<Instruction::DELETE>({recursionWasExecutable2}),
                                    makeSentence<Instruction::DELETE>({recursionTrackingLength}),
                                    makeSentence<Instruction::DELETE>({equals}),
                                    makeSentence<Instruction::DELETE>({notEquals}),
                                },
                                makeSubprogram(recursionSubprogramSpace, std::vector<Register>{
//                                        makeSentence<Instruction::DELETE_PROGRAM>({manipulateSubprogramSpace}),
                                        makeSentence<Instruction::LOOP>(
                                                {loopResult, loopSubprogramReference, inputSize}),
                                }),
                                makeSubprogram(manipulateSubprogramSpace, std::vector<Register>{
//                                        makeSentence<Instruction::DELETE_PROGRAM>({recursionSubprogramSpace}),
                                        makeSentence<Instruction::MANIPULATE>({recursionTracking}),
                                }),
                                std::vector<Register>{
                                        makeSentence<Instruction::CONCATENATE>(
                                                {recursionTracking, World::ITERATION_REGISTER}),
                                        makeSentence<Instruction::WHAT_IS_MY_REFERENCE>({whatIsMyReference}),
                                        makeSentence<Instruction::COPY_TO_SPACE>(
                                                {loopSubprogramReference, whatIsMyReference, recursionSubprogramSpace}),
                                        makeSentence<Instruction::COPY_TO_SPACE>(
                                                {inputSize, inputSize, recursionSubprogramSpace}),
                                        makeSentence<Instruction::COPY_TO_SPACE>(
                                                {recursionTracking, recursionTracking, recursionSubprogramSpace}),
//                                        makeSentence<Instruction::COPY_TO_SPACE>(
//                                                {manipulateSubprogramSpace, manipulateSubprogramSpace,
//                                                        recursionSubprogramSpace}),

                                        makeSentence<Instruction::COPY_TO_SPACE>(
                                                {recursionTracking, recursionTracking, manipulateSubprogramSpace}),
//                                        makeSentence<Instruction::COPY_TO_SPACE>(
//                                                {recursionSubprogramSpace, recursionSubprogramSpace,
//                                                        manipulateSubprogramSpace}),

                                        makeSentence<Instruction::CANCEL_CALL_SUBPROGRAM_ASYNC>(
                                                {loopWasExecutable, whatIsMyReference}),
                                        makeSentence<Instruction::LENGTH>({recursionTrackingLength, recursionTracking}),
                                        makeSentence<Instruction::EQUALS>({equals, inputSize, recursionTrackingLength}),
                                        makeSentence<Instruction::NOT>({notEquals, equals}),
                                        makeSentence<Instruction::CALL_SUBPROGRAM_IF>(
                                                {recursionSubprogramSpace, notEquals}),
                                        makeSentence<Instruction::CALL_SUBPROGRAM_ASYNC_IF>(
                                                {manipulateWasExecutable, manipulateSubprogramSpace, equals}),
                                }
                        )),
                        std::vector<Register>{
                                makeSentence<Instruction::DELETE>({whatIsMyReference}),
                                makeSentence<Instruction::DELETE>({inputSize}),
                                makeSentence<Instruction::DELETE>({loopResult}),
                                makeSentence<Instruction::WHAT_IS_MY_REFERENCE>({whatIsMyReference}),
                                makeSentence<Instruction::REQUEST_INPUT_SIZE>({inputSize}),
                                makeSentence<Instruction::COPY_TO_SPACE>(
                                        {inputSize, inputSize, subprogram}),
                                makeSentence<Instruction::LOOP>({loopResult, subprogram, inputSize}),
                                makeSentence<Instruction::CANCEL_CALL_SUBPROGRAM_ASYNC>(
                                        {toplevelWasExecutable, whatIsMyReference}),
                        }
                );


                World world{instructions, std::make_unique<SudokuRow>()};
                ASSERT_OR_THROW(not world.getProblem().isSolved());
                LOG_DEBUG("new solution provided -----");
                size_t maxAttempts = 10000;
                for (size_t i = 0; i < maxAttempts; ++i) {
                    world.iterate();
                    if (world.getProblem().isSolved()) {
                        LOG_INFO("after %lu iterations, the problem was solved", i);
                        break;
                    }
                }
                ASSERT_OR_THROW_MSG(world.getProblem().isSolved(),
                        "after " + std::to_string(maxAttempts) + " attempts the problem should be solved");
            },
            []() {
                SudokuRow problem;
                auto inputSize = problem.getInputSize();
                std::vector<long> solution;
                bool solved = false;
                LOG_DEBUG("new solution provided -----");
                for (long i = 1; i <= inputSize; ++i) {
                    std::tie(solved, solution) = subprogram(inputSize, i, {}, problem);
                    if (solved) {
                        break;
                    }
                }
                LOG_INFO("solution: %s", randomize::utils::container_to_string_ss(solution).c_str());
                ASSERT_OR_THROW(problem.isSolved());
                ASSERT_EQUALS_OR_THROW(problem.isSolved(), solved);
            }
    }};

    return 0;
}


