/**
 * @file sentence.test.cpp
 */
#include <test/TestSuite.h>
#include <world/Tools.h>

using randomize::test::TestSuite;
int main(int argc, char **argv) {
    LOG_LEVEL(randomize::log::parseLogLevel(argc > 1? argv[1] : "INFO"));

    TestSuite{"binary search", {
            []() {
                auto f = [](long int input) { return -input * input; };
                long int max = maxWithin(f, -10, 10);
                ASSERT_EQUALS_OR_THROW(max, 0);
                ASSERT_EQUALS_OR_THROW(f(max), 0);
            },
            []() {
                auto f = [](long int input) { input -= 500; return 100 - input*input;};
                long int max = maxWithin(f, -10, 1000);
                ASSERT_EQUALS_OR_THROW(max, 500);
                ASSERT_EQUALS_OR_THROW(f(max), 100);
            },
    }};
    TestSuite{"exponential search", {
            []() {
                auto f = [](long int input) { return -input*input;};
                long int max = maximize(f);
                ASSERT_EQUALS_OR_THROW(max, 0);
                ASSERT_EQUALS_OR_THROW(f(max), 0);
            },
            []() {
                auto f = [](long int input) { return 100 - input*input;};
                long int max = maximize(f);
                ASSERT_EQUALS_OR_THROW(max, 0);
                ASSERT_EQUALS_OR_THROW(f(max), 100);
            },
            []() {
                auto f = [](long int input) { input -= 50; return 100 - input*input;};
                long int max = maximize(f);
                ASSERT_EQUALS_OR_THROW(max, 50);
                ASSERT_EQUALS_OR_THROW(f(max), 100);
            },
            []() {
                auto f = [](long int input) { input -= 500; return 100 - input*input;};
                long int max = maximize(f);
                ASSERT_EQUALS_OR_THROW(max, 500);
                ASSERT_EQUALS_OR_THROW(f(max), 100);
            },
            []() {
                auto f = [](long int input) { input += 500; return 100 - input*input;};
                long int max = maximize(f);
                ASSERT_EQUALS_OR_THROW(max, -500);
                ASSERT_EQUALS_OR_THROW(f(max), 100);
            },
            []() {
                auto f = [](long int input) { input += 501; return 100 - input*input;};
                long int max = maximize(f);
                ASSERT_EQUALS_OR_THROW(max, -501);
                ASSERT_EQUALS_OR_THROW(f(max), 100);
            },
            []() {
                auto f = [](long int input) { return 0;};
                try {
                    maximize(f);
                    ASSERT_OR_THROW_MSG(false, "expected exception was not thrown");
                } catch (std::exception &e) {
                    ;
                }
            },
    }};

    return 0;
}

