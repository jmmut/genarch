/**
 * @file basic.test.cpp
 */
#include <test/TestSuite.h>
#include <world/World.h>
#include <problems/GiveMe100Derivable.h>

using randomize::test::TestSuite;

bool areFloatsEqual(double a, double b) {
    double delta = 0.00001;
    return std::abs(a - b) < delta;
}

int main(int argc, char **argv) {
    LOG_LEVEL(randomize::log::parseLogLevel(argc > 1? argv[1] : "INFO"));

    Reference generalReturnReference = 1;
    Reference randomMin = 2;
    Reference randomMax = 3;
    Reference random = 4;
    Reference myReference = 5;
    Reference copiedProgramReference = 6;
    Reference inputSize = 7;
    Reference input = 8;
    Reference output = 9;
    std::vector<Register> firstSubprogram{
            makeSentence<Instruction::DELETE>({myReference}),
            makeSentence<Instruction::DELETE>({randomMin}),
            makeSentence<Instruction::DELETE>({randomMax}),
            makeSentence<Instruction::DELETE>({random}),
            makeSentence<Instruction::DELETE>({generalReturnReference}),
            makeSentence<Instruction::DELETE>({copiedProgramReference}),
            makeSentence<Instruction::DELETE>({inputSize}),
            makeSentence<Instruction::DELETE>({input}),
            makeSentence<Instruction::DELETE>({output}),
            makeSentence<Instruction::WHAT_IS_MY_REFERENCE>({myReference}),
            makeSentence<Instruction::STORE>({randomMin, -1000}),
            makeSentence<Instruction::STORE>({randomMax, 1000}),
            makeSentence<Instruction::GET_RANDOM>({random, randomMin, randomMax}),
            makeSentence<Instruction::COPY_PROGRAM>({copiedProgramReference, myReference}),
            makeSentence<Instruction::CHANGE_VALUE_OF_PROGRAM>(
                    {generalReturnReference, copiedProgramReference,
                            static_cast<long>(Instruction::STORE_N_COPIES), 2, random}),
            makeSentence<Instruction::REQUEST_INPUT_SIZE>({inputSize}),
            makeSentence<Instruction::STORE_N_COPIES>({input, 0, inputSize}),
            makeSentence<Instruction::MANIPULATE>({input}),
            makeSentence<Instruction::PERCEIVE>({output}),
    };

    Reference max = 10;
    Reference subsubprogramSpace = 11;
    Reference inputRegister = 14;
    Reference outputRegister = 15;
    std::vector<Register> secondSubprogram{
            makeSentence<Instruction::CREATE_SUBPROGRAM_SPACE>({subsubprogramSpace}),
            makeSentence<Instruction::ADD_TO_SUBPROGRAM>(
                    {subsubprogramSpace, makeSentence<Instruction::MANIPULATE>({inputRegister})}),
            makeSentence<Instruction::ADD_TO_SUBPROGRAM>(
                    {subsubprogramSpace, makeSentence<Instruction::DELETE>({outputRegister})}),
            makeSentence<Instruction::ADD_TO_SUBPROGRAM>(
                    {subsubprogramSpace, makeSentence<Instruction::PERCEIVE>({outputRegister})}),
            makeSentence<Instruction::MAXIMIZE>({max, inputRegister, outputRegister, subsubprogramSpace}),
            makeSentence<Instruction::MANIPULATE>({max}),
            makeSentence<Instruction::PERCEIVE>({outputRegister}),
    };


    Reference firstSubprogramSpace = 16;
    Reference firstWasExecutable = 17;
    Reference secondSubprogramSpace = 18;
    Reference secondWasExecutable = 19;
    std::vector<Register> instructions;

    {
        std::vector<Register> firstSubprogramGenerator = makeSubprogram(firstSubprogramSpace, firstSubprogram);
        std::vector<Register> secondSubprogramGenerator = makeSubprogram(secondSubprogramSpace,
                secondSubprogram);
        instructions.insert(instructions.end(), firstSubprogramGenerator.begin(),
                firstSubprogramGenerator.end());
        instructions.insert(instructions.end(), secondSubprogramGenerator.begin(),
                secondSubprogramGenerator.end());

        Reference toplevelProgram = 5;
        Reference toplevelProgramWasExecutable = 6;
        instructions.push_back(makeSentence<Instruction::WHAT_IS_MY_REFERENCE>({toplevelProgram}));
        instructions.push_back(makeSentence<Instruction::CANCEL_CALL_SUBPROGRAM_ASYNC>(
                {toplevelProgramWasExecutable, toplevelProgram}));
        instructions.push_back(
                makeSentence<Instruction::CALL_SUBPROGRAM_ASYNC>({firstWasExecutable, firstSubprogramSpace}));
        instructions.push_back(
                makeSentence<Instruction::CALL_SUBPROGRAM_ASYNC>({secondWasExecutable, secondSubprogramSpace}));

        TestSuite{"same program can solve different problems", {
                [&]() {
                    {
                        World world{instructions, std::make_unique<GiveMe100Twice>()};
                        ASSERT_OR_THROW(not world.getProblem().isSolved());
                        world.logMemory();
                        world.iterate();    // first iteration creates subprograms only
                        size_t attempts = 10000;
                        for (size_t i = 0; i < attempts; ++i) { // subsequent iterations execute subprograms
                            world.iterate();
                            if (world.getProblem().isSolved()) {
                                break;
                            }
                        }
                        world.logMemory();
                        ASSERT_OR_THROW(world.getProblem().isSolved());
                    }
                },
                [&]() {
                    {
                        World world{instructions, std::make_unique<GiveMe100Derivable>()};
                        ASSERT_OR_THROW(not world.getProblem().isSolved());
                        world.logMemory();
                        world.iterate();    // first iteration creates subprograms only
                        world.iterate();    // second iteration executes subprograms
                        world.logMemory();
                        ASSERT_OR_THROW(world.getProblem().isSolved());
                    }

                }
        }};
    }

    {

        std::vector<Register> reorderedInstructions;
        // see how the second subprogram handles the wrong problem
        std::vector<Register> firstSubprogramGenerator = makeSubprogram(firstSubprogramSpace,
                firstSubprogram);
        std::vector<Register> secondSubprogramGenerator = makeSubprogram(secondSubprogramSpace,
                secondSubprogram);
        reorderedInstructions.insert(reorderedInstructions.end(), secondSubprogramGenerator.begin(),
                secondSubprogramGenerator.end());
        reorderedInstructions.insert(reorderedInstructions.end(), firstSubprogramGenerator.begin(),
                firstSubprogramGenerator.end());

        Reference toplevelProgram = 5;
        Reference toplevelProgramWasExecutable = 6;
        reorderedInstructions.push_back(
                makeSentence<Instruction::WHAT_IS_MY_REFERENCE>({toplevelProgram}));
        reorderedInstructions.push_back(makeSentence<Instruction::CANCEL_CALL_SUBPROGRAM_ASYNC>(
                {toplevelProgramWasExecutable, toplevelProgram}));
        reorderedInstructions.push_back(
                makeSentence<Instruction::CALL_SUBPROGRAM_ASYNC>({firstWasExecutable, firstSubprogramSpace}));
        reorderedInstructions.push_back(
                makeSentence<Instruction::CALL_SUBPROGRAM_ASYNC>(
                        {secondWasExecutable, secondSubprogramSpace}));

        TestSuite{"same program (reordered) can solve different problems", {
                [&]() {
                    {
                        World world{reorderedInstructions, std::make_unique<GiveMe100>()};
                        ASSERT_OR_THROW(not world.getProblem().isSolved());
                        world.logMemory();
                        world.iterate();    // first iteration creates subprograms only
                        size_t attempts = 10000;
                        for (size_t i = 0; i < attempts; ++i) { // subsequent iterations execute subprograms
                            world.iterate();
                            if (world.getProblem().isSolved()) {
                                break;
                            }
                        }
                        world.logMemory();
                        ASSERT_OR_THROW(world.getProblem().isSolved());
                    }
                },
                [&]() {
                    {
                        World world{reorderedInstructions, std::make_unique<GiveMe100Twice>()};
                        ASSERT_OR_THROW(not world.getProblem().isSolved());
                        world.logMemory();
                        world.iterate();    // first iteration creates subprograms only
                        size_t attempts = 10000;
                        for (size_t i = 0; i < attempts; ++i) { // subsequent iterations execute subprograms
                            world.iterate();
                            if (world.getProblem().isSolved()) {
                                break;
                            }
                        }
                        world.logMemory();
                        ASSERT_OR_THROW(world.getProblem().isSolved());
                    }
                },
        }};
    }
    return 0;
}

