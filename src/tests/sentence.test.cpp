/**
 * @file sentence.test.cpp
 */
#include <test/TestSuite.h>
#include <world/World.h>
#include <executors/Executor.h>

using randomize::test::TestSuite;

#define ASSERT_EQUALS_OR_THROW_SS(actual, expected) ASSERT_EQUALS_OR_THROW_SS_MSG((actual), (expected), "")

#define ASSERT_EQUALS_OR_THROW_SS_MSG(actual, expected, message) \
    if (const auto &actualResult{actual}; true) \
        if (const auto &expectedResult{expected}; not ((actualResult) == (expectedResult))) \
            if (std::stringstream comparisonMessage; true) \
                throw randomize::utils::exception::StackTracedException( \
                    ((comparisonMessage << __FILE__ << ":" << __LINE__ \
                        << (": assertion (" #actual " == " #expected ") failed: ") \
                        << actualResult << " == " << expectedResult \
                        << ". " << message), comparisonMessage.str()))


namespace std {
    string to_string(const Register &vec) {
        return ::to_string(vec);
    }
    string to_string(const std::vector<Register> &vec) {
        return randomize::utils::container_to_string_ss(vec);
    }
}
int main(int argc, char **argv) {
    LOG_LEVEL(randomize::log::parseLogLevel(argc > 1? argv[1] : "INFO"));

    TestSuite{"parse sentence", {
            []() {
                auto [success, parsed] = parseSentence<Instruction::WHAT_IS_MY_REFERENCE>(
                        makeSentence<Instruction::WHAT_IS_MY_REFERENCE>({5}));
                ASSERT_OR_THROW(success);
                ASSERT_EQUALS_OR_THROW(parsed.result, 5);
            },
            []() {
                auto sentence = makeSentence<Instruction::ADD_TO_SUBPROGRAM>(
                        {1, makeSentence<Instruction::COPY_PROGRAM>({2, 3})});
                auto [success, parsed] = parseSentence<Instruction::ADD_TO_SUBPROGRAM>(sentence);
                ASSERT_OR_THROW(success);
                ASSERT_EQUALS_OR_THROW(parsed.result, 1);
            },
    }};
    TestSuite{"make subprogram", {
            []() {
                Reference myReference = 10;
                Reference subprogramSpace = 11;
                Reference inputRegister = 14;
                Reference outputRegister = 15;

                // compare this version, with all together:
                std::vector<Register> manualInstructions{
                        makeSentence<Instruction::WHAT_IS_MY_REFERENCE>({myReference}),
                        makeSentence<Instruction::CREATE_SUBPROGRAM_SPACE>({subprogramSpace}),
                        makeSentence<Instruction::ADD_TO_SUBPROGRAM>({subprogramSpace,
                                makeSentence<Instruction::MANIPULATE>({inputRegister})}),
                        makeSentence<Instruction::ADD_TO_SUBPROGRAM>({subprogramSpace,
                                makeSentence<Instruction::DELETE>({outputRegister})}),
                        makeSentence<Instruction::ADD_TO_SUBPROGRAM>({subprogramSpace,
                                makeSentence<Instruction::PERCEIVE>({outputRegister})}),
                };


                // with this version, split by subprograms:
                std::vector<Register> automaticSubprogram{
                    makeSentence<Instruction::WHAT_IS_MY_REFERENCE>({myReference})};

                std::vector<Register> subprogramSimpleInstructions{
                        makeSentence<Instruction::MANIPULATE>({inputRegister}),
                        makeSentence<Instruction::DELETE>({outputRegister}),
                        makeSentence<Instruction::PERCEIVE>({outputRegister}),
                };
                std::vector<Register> subprogramInstructions = makeSubprogram(subprogramSpace,
                        subprogramSimpleInstructions);

                automaticSubprogram.insert(automaticSubprogram.end(), subprogramInstructions.begin(),
                        subprogramInstructions.end());

                // comparison ends here

                ASSERT_EQUALS_OR_THROW(automaticSubprogram, manualInstructions);
            }
    }};

    return 0;
}

