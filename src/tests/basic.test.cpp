/**
 * @file basic.test.cpp
 */
#include <test/TestSuite.h>
#include <world/World.h>
#include <problems/GiveMe100Derivable.h>

using randomize::test::TestSuite;

bool areFloatsEqual(double a, double b) {
    double delta = 0.00001;
    return std::abs(a - b) < delta;
}

int main(int argc, char **argv) {
    LOG_LEVEL(randomize::log::parseLogLevel(argc > 1? argv[1] : "INFO"));

    TestSuite{"simplest working program", {
            []() {
                Reference generalReturnReference = 1;
                Reference randomMin = 2;
                Reference randomMax = 3;
                Reference random = 4;
                Reference whatIsMyReference = 5;
                Reference copiedProgramReference = 6;
                Reference inputSize = 7;
                Reference input = 8;
                Reference output = 9;
                std::vector<Register> instructions{
                        makeSentence<Instruction::DELETE>({whatIsMyReference}),
                        makeSentence<Instruction::DELETE>({randomMin}),
                        makeSentence<Instruction::DELETE>({randomMax}),
                        makeSentence<Instruction::DELETE>({random}),
                        makeSentence<Instruction::DELETE>({generalReturnReference}),
                        makeSentence<Instruction::DELETE>({copiedProgramReference}),
                        makeSentence<Instruction::DELETE>({inputSize}),
                        makeSentence<Instruction::DELETE>({input}),
                        makeSentence<Instruction::DELETE>({output}),
                        makeSentence<Instruction::WHAT_IS_MY_REFERENCE>({whatIsMyReference}),
                        makeSentence<Instruction::STORE>({randomMin, -1000}),
                        makeSentence<Instruction::STORE>({randomMax, 1000}),
                        makeSentence<Instruction::GET_RANDOM>({random, randomMin, randomMax}),
                        makeSentence<Instruction::COPY_PROGRAM>({copiedProgramReference, whatIsMyReference}),
                        makeSentence<Instruction::CHANGE_VALUE_OF_PROGRAM>(
                                {generalReturnReference, copiedProgramReference,
                                        static_cast<long>(Instruction::STORE_N_COPIES), 2, random}),
                        makeSentence<Instruction::REQUEST_INPUT_SIZE>({inputSize}),
                        makeSentence<Instruction::STORE_N_COPIES>({input, 100, inputSize}),
                        makeSentence<Instruction::MANIPULATE>({input}),
                        makeSentence<Instruction::PERCEIVE>({output})
                };

                World world{instructions, std::make_unique<GiveMe100Twice>()};
                ASSERT_OR_THROW(not world.getProblem().isSolved());
                world.iterate();
                world.logMemory();
                ASSERT_OR_THROW(world.getProblem().isSolved());
            }
    }};

    TestSuite{"simplest self-modifying program", {
            []() {
                Reference generalReturnReference = 1;
                Reference randomMin = 2;
                Reference randomMax = 3;
                Reference random = 4;
                Reference whatIsMyReference = 5;
                Reference copiedProgramReference = 6;
                Reference inputSize = 7;
                Reference input = 8;
                Reference output = 9;
                std::vector<Register> instructions{
                        makeSentence<Instruction::DELETE>({whatIsMyReference}),
                        makeSentence<Instruction::DELETE>({randomMin}),
                        makeSentence<Instruction::DELETE>({randomMax}),
                        makeSentence<Instruction::DELETE>({random}),
                        makeSentence<Instruction::DELETE>({generalReturnReference}),
                        makeSentence<Instruction::DELETE>({copiedProgramReference}),
                        makeSentence<Instruction::DELETE>({inputSize}),
                        makeSentence<Instruction::DELETE>({input}),
                        makeSentence<Instruction::DELETE>({output}),
                        makeSentence<Instruction::WHAT_IS_MY_REFERENCE>({whatIsMyReference}),
                        makeSentence<Instruction::STORE>({randomMin, -1000}),
                        makeSentence<Instruction::STORE>({randomMax, 1000}),
                        makeSentence<Instruction::GET_RANDOM>({random, randomMin, randomMax}),
                        makeSentence<Instruction::COPY_PROGRAM>({copiedProgramReference, whatIsMyReference}),
                        makeSentence<Instruction::CHANGE_VALUE_OF_PROGRAM>(
                                {generalReturnReference, copiedProgramReference,
                                        static_cast<long>(Instruction::STORE_N_COPIES), 2, random}),
                        makeSentence<Instruction::REQUEST_INPUT_SIZE>({inputSize}),
                        makeSentence<Instruction::STORE_N_COPIES>({input, 0, inputSize}),
                        makeSentence<Instruction::MANIPULATE>({input}),
                        makeSentence<Instruction::PERCEIVE>({output})
                };

                World world{instructions, std::make_unique<GiveMe100Twice>()};
                ASSERT_OR_THROW(not world.getProblem().isSolved());
                size_t attempts = 10000;
                for (size_t i = 0; i < attempts; ++i) {
                    world.iterate();
                    if (world.getProblem().isSolved()) {
                        break;
                    }
                }
                ASSERT_OR_THROW_MSG(world.getProblem().isSolved(),
                        "after " + std::to_string(attempts) + " attempts the problem should be solved");
            }
    }};

    TestSuite{"self-modifying program for derivable problem space", {
            []() {
                GiveMe100Derivable problem;
                problem.manipulate({100});
                ASSERT_EQUALS_OR_THROW(problem.getOutput()[0], 1000);
            },
            []() {
                GiveMe100Derivable problem;
                problem.manipulate({99});
                ASSERT_EQUALS_OR_THROW(problem.getOutput()[0], 500);
            },
            []() {
                GiveMe100Derivable problem;
                problem.manipulate({101});
                ASSERT_EQUALS_OR_THROW(problem.getOutput()[0], 500);
            },
            []() {
                GiveMe100Derivable problem;
                problem.manipulate({90});
                ASSERT_EQUALS_OR_THROW(problem.getOutput()[0], 90);
            },
            []() {
                GiveMe100Derivable problem;
                problem.manipulate({0});
                ASSERT_EQUALS_OR_THROW(problem.getOutput()[0], 9);
            },
            []() {
                GiveMe100Derivable problem;
                problem.manipulate({-100});
                ASSERT_EQUALS_OR_THROW(problem.getOutput()[0], 4);
            },
            []() {
                Reference subprogramSpace = 11;
                Reference max = 13;
                Reference inputRegister = 14;
                Reference outputRegister = 15;
                std::vector<Register> instructions{
                        makeSentence<Instruction::CREATE_SUBPROGRAM_SPACE>({subprogramSpace}),
                        makeSentence<Instruction::ADD_TO_SUBPROGRAM>(
                                {subprogramSpace, makeSentence<Instruction::MANIPULATE>({inputRegister})}),
                        makeSentence<Instruction::ADD_TO_SUBPROGRAM>(
                                {subprogramSpace, makeSentence<Instruction::DELETE>({outputRegister})}),
                        makeSentence<Instruction::ADD_TO_SUBPROGRAM>(
                                {subprogramSpace, makeSentence<Instruction::PERCEIVE>({outputRegister})}),
                        makeSentence<Instruction::MAXIMIZE>({max, inputRegister, outputRegister, subprogramSpace}),
                        makeSentence<Instruction::MANIPULATE>({max}),
                        makeSentence<Instruction::PERCEIVE>({outputRegister}),
                };

                World world{instructions, std::make_unique<GiveMe100Derivable>()};
                ASSERT_OR_THROW(not world.getProblem().isSolved());
                size_t attempts = 1000;
                for (size_t i = 0; i < attempts; ++i) {
                    world.iterate();
                    if (world.getProblem().isSolved()) {
                        break;
                    }
                }
                ASSERT_OR_THROW_MSG(world.getProblem().isSolved(),
                        "after " + std::to_string(attempts) + " attempts the problem should be solved");
            }
    }};

    return 0;
}

