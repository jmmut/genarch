/**
 * @file basic.test.cpp
 */
#include <test/TestSuite.h>
#include <world/World.h>
#include <problems/GiveMe100Derivable.h>
#include <functional>

using randomize::test::TestSuite;

bool areFloatsEqual(double a, double b) {
    double delta = 0.00001;
    return (a - b) < delta;
}

int main(int argc, char **argv) {
    LOG_LEVEL(randomize::log::parseLogLevel(argc > 1? argv[1] : "INFO"));

    TestSuite{"subprogram (reflection)", {
            []() {
                Reference rangeMinReference = 1;
                Reference rangeMaxReference = 2;
//                Value rangeMinValue = -100000;
                Value rangeMinValue = -1000;
//                Value rangeMaxValue = 100000;
                Value rangeMaxValue = 1000;

                Reference topSpace = 5;
                Reference random = 9;
                Reference copiedProgramReference = 6;
                Reference inputSize = 13;
                Reference input = 14;
                Reference output = 15;

                Reference oldValue = 11;
                Value subprogramSpace = 4;
                Reference programReferenceReference = 3;
                Reference randomInSubprogramSpace = 7;
                std::vector<Register> instructions{
                        makeSentence<Instruction::DELETE>({topSpace}),
                        makeSentence<Instruction::DELETE>({rangeMinReference}),
                        makeSentence<Instruction::DELETE>({rangeMaxReference}),
                        makeSentence<Instruction::DELETE>({random}),
                        makeSentence<Instruction::DELETE>({subprogramSpace}),
                        makeSentence<Instruction::DELETE>({inputSize}),
                        makeSentence<Instruction::DELETE>({input}),
                        makeSentence<Instruction::DELETE>({output}),
                        makeSentence<Instruction::WHAT_IS_MY_REFERENCE>({topSpace}),
                        makeSentence<Instruction::STORE_IN_SPACE>({rangeMinReference, rangeMinValue, topSpace}),
                        makeSentence<Instruction::STORE_IN_SPACE>({rangeMaxReference, rangeMaxValue, topSpace}),
                        makeSentence<Instruction::GET_RANDOM>({random, rangeMinReference, rangeMaxReference}),
                        makeSentence<Instruction::CREATE_SUBPROGRAM_SPACE>({subprogramSpace}),
                        makeSentence<Instruction::COPY_TO_SPACE>({programReferenceReference, topSpace,
                                subprogramSpace}),
                        makeSentence<Instruction::COPY_TO_SPACE>({randomInSubprogramSpace, random,
                                subprogramSpace}),
                        makeSentence<Instruction::ADD_TO_SUBPROGRAM>(
                                {subprogramSpace, makeSentence<Instruction::COPY_PROGRAM>({
                                        copiedProgramReference, programReferenceReference})}),
                        makeSentence<Instruction::ADD_TO_SUBPROGRAM>(
                                {subprogramSpace, makeSentence<Instruction::CHANGE_VALUE_OF_PROGRAM>({
                                        oldValue, copiedProgramReference,
                                        static_cast<long>(Instruction::STORE_N_COPIES),
                                        2, randomInSubprogramSpace})}),
                        makeSentence<Instruction::CALL_SUBPROGRAM>({subprogramSpace}),

                        makeSentence<Instruction::REQUEST_INPUT_SIZE>({inputSize}),

                        // this 0 will be changed randomly by the program itself
                        makeSentence<Instruction::STORE_N_COPIES>({input, 0, inputSize}),

                        makeSentence<Instruction::MANIPULATE>({input}),
                        makeSentence<Instruction::PERCEIVE>({output})
                };
                World world{instructions, std::make_unique<GiveMe100>()};
                /*
                ASSERT_OR_THROW(not world.getProblem().isSolved());
                size_t attempts = 10000;
                for (size_t i = 0; i < attempts; ++i) {
                    world.iterate();
                    if (world.getProblem().isSolved()) {
                        break;
                    }
                }
                ASSERT_OR_THROW_MSG(world.getProblem().isSolved(),
                        "after " + std::to_string(attempts) + " attempts the problem should be solved");

                */
            }
    }};
    return 0;
}

