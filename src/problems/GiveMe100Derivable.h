/**
 * @file GiveMe100.h
 */

#ifndef GENARCH_GIVEME100DERIVABLE_H
#define GENARCH_GIVEME100DERIVABLE_H

#include <cmath>
#include "Problem.h"

/**
 * output is an integer between 0 and 1000. 1000 means the correct answer. The lower the output, the higher the error.
 */
class GiveMe100Derivable : public Problem {
public:
    GiveMe100Derivable() : status{NOT_STARTED}, output{0} {}
    ~GiveMe100Derivable() override = default;

    InputSize getInputSize() override {
        return 1;
    }

    bool manipulate(const Input &input) override {
        if (input.size() != static_cast<size_t>(getInputSize())) {
            return false;
        } else {
            auto diff = std::abs(100L - input[0]);
            output = 1000.0 / (1.0 + diff);
            if (input[0] == 100) {
                status = SOLVED;
            } else {
                status = FAILED;
            }
            return true;
        }
    }

    Output getOutput() override {
        return {output};
    }

    bool isSolved() override{
        return status == SOLVED;
    }
    bool isFinished() override{
        return status == SOLVED or status == FAILED;
    }

private:
    enum Status {
        NOT_STARTED,
        FAILED,
        SOLVED
    };

    Status status;
    long output;
};

#endif //GENARCH_GIVEME100DERIVABLE_H
