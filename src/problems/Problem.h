/**
 * @file Problem.h
 */

#ifndef GENARCH_PROBLEM_H
#define GENARCH_PROBLEM_H

#include <vector>

using InputSize = long;
using Input = std::vector<long>;
using Output = std::vector<long>;

class Problem {
public:
    virtual ~Problem() = default;

    /**
     * An input to a problem is a solution candidate. The solution candidate is a list of numbers.
     * This function returns how many numbers are needed to specify a solution.
     * Example: if a solution to a problem is (100, 100), the input size is 2.
     * Note that solutions may be partial. A videogame needs several interactions across time; each interaction is a
     * partial solution.
     */
    virtual InputSize getInputSize() = 0;

    /**
     * A problem may or may not be parameterized.
     * Parameterized example: fibonacci(n) has parameter size 1.
     * Non-parameterized example: 5! has parameter size 0.
     * Multiparameterized example: multiply(x, y) has parameter size 2.
     */
     // TODO
//    virtual InputSize getParameterSize() = 0;

    /**
     * Returns false if the input had the wrong size. Returns true otherwise, even if the input didn't solve the problem
     */
     virtual bool manipulate(const Input &input) = 0;
     // TODO
//    virtual bool manipulate(const Input &input) { return manipulate(input, {}); };
//    virtual bool manipulate(const Input &input, const Input &parameters) = 0;

    /**
     * Returns the status of a problem. For instance, a half-finished sudoku could return the grid with both numbers
     * and blanks. A stateless problem like fibonacci could return anything, like whether the last solution was valid.
     */
    virtual Output getOutput() = 0;

    virtual bool isSolved() = 0;
    virtual bool isFinished() = 0;
};


#endif //GENARCH_PROBLEM_H
