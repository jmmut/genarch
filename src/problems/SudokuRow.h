/**
 * @file SudokuRow.h
 */

#ifndef GENARCH_SUDOKU_ROW_H
#define GENARCH_SUDOKU_ROW_H

#include <set>
#include "Problem.h"

class SudokuRow : public Problem {
public:
    SudokuRow() : status{NOT_STARTED}, output{getInputSize(), 0} {}
    ~SudokuRow() override = default;

    InputSize getInputSize() override {
        return 5;
    }

    bool manipulate(const Input &input) override {
        LOG_DEBUG("solution provided: %s", randomize::utils::container_to_string_ss(input).c_str());
        if (input.size() != static_cast<size_t>(getInputSize())) {
            return false;
        } else {
            output = input;
            std::set<long> numbersPresent;
            for (const auto &cellContent : input) {
                if (not inRange(cellContent)) {
                    status = FAILED;
                    return true;
                }
                if (cellContent != 0) {
                    numbersPresent.insert(cellContent);
                }
            }
            if (numbersPresent.size() == static_cast<size_t>(getInputSize())) {
                status = SOLVED;
            } else {
                status = FAILED;
            }
            return true;
        }
    }

    Output getOutput() override {
        return {status};
    }

    bool isSolved() override{
        return status == SOLVED;
    }
    bool isFinished() override{
        return status == SOLVED or status == FAILED;
    }

private:

    /** Consider 1 to getInputSize() a valid range and 0 as "not provided" (valid too) */
    bool inRange(long number) {
        return number >= 0 and number <= getInputSize();
    }

    enum Status {
        NOT_STARTED,
        IN_PROGRESS,
        FAILED,
        SOLVED
    };

    Status status;
    Output output;
};

#endif //GENARCH_SUDOKU_ROW_H
