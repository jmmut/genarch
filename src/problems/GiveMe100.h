/**
 * @file GiveMe100.h
 */

#ifndef GENARCH_GIVEME100_H
#define GENARCH_GIVEME100_H

#include "Problem.h"

class GiveMe100 : public Problem {
public:
    GiveMe100() : status{NOT_STARTED} {}
    ~GiveMe100() override = default;

    InputSize getInputSize() override {
        return 1;
    }

    bool manipulate(const Input &input) override {
        if (input.size() != static_cast<size_t>(getInputSize())) {
            return false;
        } else {
            if (input[0] == 100) {
                status = SOLVED;
            } else {
                status = FAILED;
            }
            return true;
        }
    }

    Output getOutput() override {
        return {status};
    }

    bool isSolved() override{
        return status == SOLVED;
    }
    bool isFinished() override{
        return status == SOLVED or status == FAILED;
    }

private:
    enum Status {
        NOT_STARTED,
        FAILED,
        SOLVED
    };

    Status status;
};

#endif //GENARCH_GIVEME100_H
