/**
 * @file Executor.h
 */

#ifndef GENARCH_EXECUTOR_H
#define GENARCH_EXECUTOR_H

#include <genome/Genome.h>
#include <memory>
#include <world/World.h>
#include <world/Tools.h>
#include <random>
#include <algorithm>

/*
class ExecutorBase {
public:
//    virtual ExecutorBase(World &world) : world{world} {}
    virtual ~ExecutorBase() = default;

    virtual auto execute() -> bool = 0;
//    virtual auto getInstruction() -> Instruction = 0;

private:
//    World &world;
};

template<Instruction MY_INSTRUCTION>
class Executor : public ExecutorBase {
public:
    using MyParameters = Parameters<MY_INSTRUCTION>;
    Executor(Reference reference, const MyParameters &params, World &world)
            :reference(reference), params(params), world(world) {}
    ~Executor() override = default;

    bool execute() override {
        return execute(world, reference, params);
    }

private:
    Reference reference;
    MyParameters params;
    World &world;
};

//template <Instruction I>
//struct ExecutorFactoryFunctor {
//    std::unique_ptr<ExecutorBase> operator()() {
//        return std::make_unique<Executor<I>>();
//    }
//};

template <Instruction I>
auto executorFactory(Reference reference, const Parameters<I> &params, World &world) -> std::unique_ptr<ExecutorBase> {
    return std::make_unique<Executor<I>>(reference, params, world);
//    callTemplated<ExecutorFactoryFunctor>(instruction, Reference reference, const Parameters<I> &params, World &world);
}
*/
/*
template<> class Executor<Instruction::WHAT_IS_MY_REFERENCE> : public ExecutorBase {
public:
    using MyParameters = Parameters<Instruction::WHAT_IS_MY_REFERENCE>;
    Executor(Reference reference, const MyParameters &params, World &world)
            :reference(reference), params(params), world(world) {}
    ~Executor() override = default;

    auto execute() -> bool override {
        return world.world.assignRegisterWithoutOverwriting(params.result, {world.getCurrentSpace()});
    }

private:
    Reference reference;
    MyParameters params;
    World &world;
};

template<> class Executor<Instruction::PROGRAM> : public ExecutorBase {
public:
    using MyParameters = Parameters<Instruction::PROGRAM>;
    Executor(Reference reference, const MyParameters &params, World &world)
            :reference(reference), params(params), world(world) {}
    ~Executor() override = default;

    auto execute() -> bool override {
        return world.executeProgram(reference, params.instructions);
    }

private:
    Reference reference;
    MyParameters params;
    World &world;
};
*/

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::WHAT_IS_MY_TOPLEVEL_REFERENCE> &params) {
    return world.assignRegisterWithoutOverwriting(params.result, {world.getExecutingProgram()});
}
inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::WHAT_IS_MY_REFERENCE> &params) {
    return world.assignRegisterWithoutOverwriting(params.result, {world.getCurrentSpace()});
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::COPY_PROGRAM> &params) {
    auto[success, subprogramSpace] = world.getRegisterValue(params.referenceToProgramForCopying, 0);
    if (not success) {
        return false;
    }


    //    Reference newProgram = copyProgram(subprogramSpace);
    auto newProgram = world.createNewProgram();
    world.memorySpaces[newProgram] = world.memorySpaces[subprogramSpace];
    return world.assignRegisterWithoutOverwriting(params.result, {newProgram});
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::COPY_PROGRAM_DEEP> &params) {
    auto[success, programSpace] = world.getRegisterValue(params.referenceToProgramForCopying, 0);
    if (not success) {
        return false;
    }

    auto [copied, copyReference] = world.copySubprogramDeep(programSpace);
    if (not copied) {
        return false;
    }
    world.makeExecutable(copyReference);

    return world.assignRegisterWithoutOverwriting(params.result, {copyReference});
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::COPY_TO_SPACE> &params) {
    auto registerWithReferenceToRegisterForCopying = world.getRegisterPtr(params.referenceToRegisterToCopy);
    if (not registerWithReferenceToRegisterForCopying) {
        return false;
    }
    auto[successSpace, memorySpace] = world.getRegisterValue(params.memorySpace, 0);
    if (not successSpace) {
        LOG_DEBUG("register %ld containing memory space does not exist", params.memorySpace);
    }
    auto registersIterator = world.memorySpaces.find(memorySpace);
    bool registersExist = registersIterator != world.memorySpaces.end();
    if (not registersExist) {
        return false;
    }
    return world.assignRegisterWithoutOverwriting(params.result, *registerWithReferenceToRegisterForCopying, memorySpace);
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::REQUEST_INPUT_SIZE> &params) {
    return world.assignRegisterWithoutOverwriting(params.result, {world.getProblem().getInputSize()});
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::MANIPULATE> &params) {
    auto [actionExists, action] = world.getRegisterCopy(params.action);
    if (not actionExists) {
        return false;
    }
    auto inputHadTheCorrectSize = world.getProblem().manipulate(action);
    if (not inputHadTheCorrectSize) {
        LOG_DEBUG("the problem received input with the wrong size %ld (should be %ld). %s", action.size(),
                world.getProblem().getInputSize(), action.size() < 20? to_string(action).c_str(): "");
    }
    return inputHadTheCorrectSize;
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::PERCEIVE> &params) {
    return world.assignRegisterWithoutOverwriting(params.result, world.getProblem().getOutput());
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::STORE> &params) {
    return world.assignRegisterWithoutOverwriting(params.result, {params.valueToStore});
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::STORE_IN_SPACE> &params) {
    auto[success, memorySpace] = world.getRegisterValue(params.memorySpace, 0);
    if (not success) {
        return false;
    }
    auto registersIterator = world.memorySpaces.find(memorySpace);
    bool registersExist = registersIterator != world.memorySpaces.end();
    if (not registersExist) {
        return false;
    }
    auto &targetRegisters = registersIterator->second;
    return world.assignRegisterWithoutOverwriting(params.result, {params.valueToStore}, memorySpace);
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::STORE_N_COPIES> &params) {
    auto[success, copies] = world.getRegisterValue(params.copiesReference, 0);
    if (not success) {
        return false;
    }

    Register repeatedValue(copies, params.valueToStore);
    return world.assignRegisterWithoutOverwriting(params.result, repeatedValue);
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::GET_RANDOM> &params) {
    static std::random_device device;
    static std::default_random_engine generator{device()};

    auto [successMin, min] = world.getRegisterValue(params.min, 0);
    auto [successMax, max] = world.getRegisterValue(params.max, 0);
    if (not successMin or not successMax) {
        return false;
    }
    std::uniform_int_distribution<long> distribution{min, max};
    auto random_number = distribution(generator);
    return world.assignRegisterWithoutOverwriting(params.result, {random_number});
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::CHANGE_VALUE> &params) {
    auto[success, targetRegisterIterator] = world.getRegisterIteratorByIndirection(params.referenceToTargetReference);
    if (not success) {
        return false;
    }
    auto &targetRegister = targetRegisterIterator->second;

    auto[success2, valueIndexToChange] = world.getRegisterValue(params.referenceToTargetValueIndex, 0);
    if (not success2) {
        return false;
    }
    if (valueIndexToChange < 0 or targetRegister.size() < static_cast<size_t>(valueIndexToChange)) {
        LOG_DEBUG("can't change %ld[%ld]: register has only %lu values",
                world.getRegisterValue(params.referenceToTargetReference, 0).second, valueIndexToChange, targetRegister.size());
        return false;
    }

    auto[success3, newValue] = world.getRegisterValue(params.referenceToNewValue, 0);
    if (not success3) {
        return false;
    }

    LOG_DEBUG("changing value at %ld[%ld] from %ld to %ld",
            world.getRegisterValue(params.referenceToTargetReference, 0).second,
            valueIndexToChange, targetRegister[valueIndexToChange], newValue);
    auto oldValue = targetRegister[valueIndexToChange];
    targetRegister[valueIndexToChange] = newValue;

    // return previous value that is being changed
    return world.assignRegisterWithoutOverwriting(params.result, {oldValue});
}

/**
 * TODO: what if the instruction appears several times in the same program? for now keep subprograms small
 */
inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::CHANGE_VALUE_OF_PROGRAM> &params) {
    auto[successProgramSpace, programSpace] = world.getRegisterValue(params.referenceToReferenceOfTargetProgram, 0);
    if (not successProgramSpace) {
        return false;
    }
    auto[success, targetProgramIterator] = world.getRegisterIterator(World::PROGRAM_REGISTER, programSpace);
    if (not success) {
        return false;
    }
    auto targetProgram = targetProgramIterator->second;

    Reference instructionToChange = -1;
    bool instructionFound = false;
    for (size_t i = 1; i < targetProgram.size(); ++i) {
        auto &instructionReference = targetProgram[i];
        auto[instructionExists, instructionCode] = world.getRegisterValue(instructionReference, 0, programSpace);
        if (not instructionExists) {
            // invalid program
            return false;
        } else if (instructionCode == params.instructionCodeToChange) {
            instructionToChange = instructionReference;
            instructionFound = true;
            break;
        }
    }
    if (not instructionFound) {
        LOG_DEBUG("can't change instruction %s because no such instruction exists in program %ld",
                to_string(static_cast<Instruction>(params.instructionCodeToChange)).c_str(), programSpace);
        return false;
    }
    auto &targetRegister = world.getRegisterIterator(instructionToChange, programSpace).second->second;

    if (params.valueIndexToChange < 0 or targetRegister.size() < static_cast<size_t>(params.valueIndexToChange)) {
        LOG_DEBUG("can't change %ld[%ld]: register has only %lu values",
                instructionToChange, params.valueIndexToChange, targetRegister.size());
        return false;
    }

    auto[success3, newValue] = world.getRegisterValue(params.referenceToNewValue, 0);
    if (not success3) {
        return false;
    }

    LOG_INFO("changing value at %ld:%ld[%ld] from %ld to %ld", programSpace, instructionToChange,
            params.valueIndexToChange, targetRegister[params.valueIndexToChange], newValue);
    auto oldValue = targetRegister[params.valueIndexToChange];
    targetRegister[params.valueIndexToChange] = newValue;

    // return previous value that is being changed
    return world.assignRegisterWithoutOverwriting(params.result, {oldValue});
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::PROGRAM> &params) {
    return world.executeSentences(reference, params.instructions);
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::CREATE_SUBPROGRAM_SPACE> &params) {
    auto newSpace = world.createNewSubprogram();
    return world.assignRegisterWithoutOverwriting(params.result, {newSpace});
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::ADD_TO_SUBPROGRAM> &params) {
    auto referenceToSubprogramSpace = params.result;
    auto [subprogramSuccess, subprogramSpace] = world.getRegisterValue(referenceToSubprogramSpace, 0);
    if (not subprogramSuccess) {
        return false;
    }
    auto &[instructionReference, instruction] = world.createNewRegisterAndGet(subprogramSpace);
    instruction = params.sentence;

    auto [success, subprogramIterator] = world.getRegisterIterator(World::PROGRAM_REGISTER, subprogramSpace);
    if (not success) {
        return false;
    }
    Register &subprogram = subprogramIterator->second;
    subprogram.push_back(instructionReference);
    return true;
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::CALL_SUBPROGRAM> &params) {
    auto[successSpace, subprogramSpace] = world.getRegisterValue(params.subprogramSpace, 0);
    if (not successSpace) {
        return false;
    }

    //    LOG_DEBUG("world before calling subprogram\n%s", to_string_memory().c_str());
    bool subprogramExecutedCorrectly = world.executeProgramReference(subprogramSpace);

    //    LOG_DEBUG("world after calling subprogram\n%s", to_string_memory().c_str());
    return subprogramExecutedCorrectly;
}

inline bool execute(World &world, const Reference &reference,
        const Parameters<Instruction::CALL_SUBPROGRAM_IF> &params) {
    auto[successCond, shouldJump] = world.getRegisterValue(params.jumpIfNonZero, 0);
    if (not successCond) {
        return false;
    }
    if (shouldJump) {
        auto[successSpace, subprogramSpace] = world.getRegisterValue(params.subprogramSpace, 0);
        if (not successSpace) {
            return false;
        }

        //    LOG_DEBUG("world before calling subprogram\n%s", to_string_memory().c_str());
        bool subprogramExecutedCorrectly = world.executeProgramReference(subprogramSpace);

        //    LOG_DEBUG("world after calling subprogram\n%s", to_string_memory().c_str());
        return subprogramExecutedCorrectly;
    } else {
        return true;
    }
}

inline bool execute(World &world, const Reference &reference,
        const Parameters<Instruction::CALL_SUBPROGRAM_ASYNC> &params) {
    auto[successSpace, subprogramSpace] = world.getRegisterValue(params.subprogram, 0);
    if (not successSpace) {
        return false;
    }
    bool wasAlreadyExecutable = world.makeExecutable(subprogramSpace);
    return world.assignRegisterWithoutOverwriting(params.wasAlreadyScheduled, {wasAlreadyExecutable});
}

inline bool execute(World &world, const Reference &reference,
        const Parameters<Instruction::CALL_SUBPROGRAM_ASYNC_IF> &params) {
    auto[successCond, shouldJump] = world.getRegisterValue(params.jumpIfNonZero, 0);
    if (not successCond) {
        return false;
    }
    if (shouldJump) {
        auto[successSpace, subprogramSpace] = world.getRegisterValue(params.subprogram, 0);
        if (not successSpace) {
            return false;
        }
        bool wasAlreadyExecutable = world.makeExecutable(subprogramSpace);
        return world.assignRegisterWithoutOverwriting(params.wasAlreadyScheduled, {wasAlreadyExecutable});
    } else {
        return true;
    }
}

inline bool execute(World &world, const Reference &reference,
        const Parameters<Instruction::CANCEL_CALL_SUBPROGRAM_ASYNC> &params) {
    auto[successSpace, subprogramSpace] = world.getRegisterValue(params.subprogram, 0);
    if (not successSpace) {
        return false;
    }
    bool wasExecutable = world.makeNonExecutable(subprogramSpace) > 0;
    return world.assignRegisterWithoutOverwriting(params.wouldHaveExecuted, {wasExecutable});
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::DELETE> &params) {
    world.getCurrentRegisters().erase(params.registerToDelete);
    return true;
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::DELETE_PROGRAM> &params) {
    auto[successSpace, subprogramSpace] = world.getRegisterValue(params.space, 0);
    if (not successSpace) {
        return false;
    }
    world.makeNonExecutable(subprogramSpace);
    world.memorySpaces.erase(subprogramSpace);
    return true;
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::MAXIMIZE> &params) {
    auto[successSpace, subprogramSpace] = world.getRegisterValue(params.subprogram, 0);
    if (not successSpace) {
        return false;
    }
    std::vector<long> evaluations;
    auto func = [&world, &evaluations, params, reference, subprogramSpace=subprogramSpace](long input) -> long {
        world.memorySpaces[subprogramSpace][params.input] = {input};
        bool subprogramExecutedCorrectly = execute(world, reference,
                Parameters<Instruction::CALL_SUBPROGRAM>{params.subprogram});
        evaluations.push_back(input);
        if (not subprogramExecutedCorrectly) {
            throw std::logic_error{"some instruction failed"};
        }
        Register &result = world.memorySpaces[subprogramSpace][params.output];
        return result[0];
    };
    long max;
    try {
        max = maximize(func);
    } catch (std::exception &e) {
        LOG_DEBUG("subprogram %ld didn't work: %s", subprogramSpace, e.what());
        return false;
    }
    LOG_DEBUG("made %ld evaluations of subprogram %ld to maximize it: %s", evaluations.size(),
            subprogramSpace, randomize::utils::container_to_string(evaluations).c_str());
    return world.assignRegisterWithoutOverwriting(params.result, {max});
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::LOOP> &params) {
    auto[successSpace, subprogramSpace] = world.getRegisterValue(params.subprogram, 0);
    if (not successSpace) {
        return false;
    }
    auto[successTimes, times] = world.getRegisterValue(params.times, 0);
    if (not successTimes) {
        return false;
    }
    for (long i = 1; i <= times; i++) {
        Reference programCopy = world.copyProgram(subprogramSpace);
        world.memorySpaces[programCopy][World::ITERATION_REGISTER] = {i};
        bool subprogramExecutedCorrectly = world.executeProgramReference(programCopy);
    }

    return true;
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::LOOP_ASYNC> &params) {
    auto[successSpace, subprogramSpace] = world.getRegisterValue(params.subprogram, 0);
    if (not successSpace) {
        return false;
    }
    auto[successTimes, times] = world.getRegisterValue(params.times, 0);
    if (not successTimes) {
        return false;
    }
    for (long i = 1; i <= times; i++) {
        Reference programCopy = world.copyProgram(subprogramSpace);
        world.memorySpaces[programCopy][World::ITERATION_REGISTER] = {i};
        world.makeExecutable(programCopy);
    }

    return true;
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::CONCATENATE> &params) {
    auto[success, toAdd] = world.getRegisterCopy(params.toAddAtTheEnd);
    if (not success) {
        return false;
    }
    auto &result = world.getCurrentRegisters()[params.result];
    result.insert(result.end(), toAdd.begin(), toAdd.end());
    return true;
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::EQUALS> &params) {
    auto[successA, a] = world.getRegisterCopy(params.a);
    if (not successA) {
        return false;
    }
    auto[successB, b] = world.getRegisterCopy(params.b);
    if (not successB) {
        return false;
    }
    bool equals = std::equal(a.begin(), a.end(), b.begin());
    return world.assignRegisterWithoutOverwriting(params.result, {equals});
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::LENGTH> &params) {
    auto[success, aRegister] = world.getRegisterCopy(params.aRegister);
    if (not success) {
        return false;
    }
    long length = std::size(aRegister);
    return world.assignRegisterWithoutOverwriting(params.result, {length});
}

inline bool execute(World &world, const Reference &reference, const Parameters<Instruction::NOT> &params) {
    auto[success, oldValue] = world.getRegisterValue(params.aRegister, 0);
    if (not success) {
        return false;
    }
    bool negated = oldValue == 0;
    return world.assignRegisterWithoutOverwriting(params.result, {negated});
}

#endif //GENARCH_EXECUTOR_H
