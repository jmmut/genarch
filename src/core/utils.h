/**
 * @file utils.h
 * @author jmmut
 * @date 2019-03-21.
 */

#ifndef EMOTIONBOTS_UTILS_H
#define EMOTIONBOTS_UTILS_H

#include <functional>

inline double inRange(double value) {
    if (value < 0) {
        return 0;
    } else if (value > 1) {
        return 1;
    } else {
        return value;
    }
}

/**
 * Don't use unnamed variables! this won't work: AtConstructionAndAtDestruction{ f1, f2 };
 * use this instead: AtConstructionAndAtDestruction instance{ f1, f2 };
 * or this: auto instance = AtConstructionAndAtDestruction{ f1, f2 };
 */
class AtConstructionAndAtDestruction {
public:
    AtConstructionAndAtDestruction (std::function<void()> init, std::function<void()> destruct) : destruct{destruct} {
        init();
    }
    virtual ~AtConstructionAndAtDestruction() {
        destruct();
    }

private:
    std::function<void()> destruct;
};
#endif //EMOTIONBOTS_UTILS_H
