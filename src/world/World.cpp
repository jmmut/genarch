/**
 * @file Genome.cpp
 */

#include <random>
#include <iomanip>
#include <utility>
#include <core/utils.h>
#include <executors/Executor.h>
#include "World.h"

template class std::vector<Reference>;  // force instantiation so that gdb can evaluate methods that might be inlined

World::World(std::vector<Register> instructions, std::unique_ptr<Problem> problem) : problem{std::move(problem)} {
    Reference instructionsStart = AUTOMATICALLY_CREATED_REGISTERS_START_AT; // TODO: better design for this.
            // - instruction for stating program space?
            // - virtualizing variables? does this forbid operations on instructions with
            //   hardcoded addresses? is this even possible once pointer arithmetic and
            //   dereference is implemented?
            // - split variable and program space? only for setting up the program?
            // - rename variable if the first time it is used it already has a value?
            //   does this forbid modifying right away any instruction with hardcoded
            //   address?
    programs.clear();
    memorySpaces.clear();
    currentSpace.clear();
    currentSpace.push_back(TOP_LEVEL_SPACE);

    Reference programReference = createNewProgram();
    Register &program = getRegisterIterator(programReference).second->second;

    for (size_t instructionIndex = 0; instructionIndex < instructions.size(); ++instructionIndex) {
        auto reference = instructionIndex + instructionsStart;
        auto vector = instructions[instructionIndex];
        getCurrentRegisters()[reference] = vector;
        program.push_back(reference);
    }
}

Reference World::createNewProgram() {
    long id = createNewSubprogram();
    programs.insert(id);
    return id;
}

Reference World::createNewSubprogram() {
    if (programs.size() >= maxPrograms) {
        std::stringstream message;
        message << "program limit (" << maxPrograms << ") reached";
        throw randomize::utils::exception::StackTracedException{message.str()};
    }
    auto space = createNewSpace();
    memorySpaces.at(space)[PROGRAM_REGISTER] = {static_cast<Reference>(Instruction::PROGRAM)};
    return space;
}

Reference World::createNewSpace() {
    if (memorySpaces.size() >= maxSpaces) {
        std::stringstream message;
        message << "memory spaces limit (" << maxSpaces << ") reached";
        throw randomize::utils::exception::StackTracedException{message.str()};
    }
    auto newSpace = lastCreatedSpaceId;
    lastCreatedSpaceId++;
    memorySpaces[newSpace];
    return newSpace;
}

Reference World::createNewRegister() {
    return createNewRegisterAndGet().first;
}

Reference World::createNewRegister(Reference memorySpace) {
    return createNewRegisterAndGet(memorySpace).first;
}

std::pair<const Reference, Register>& World::createNewRegisterAndGet() {
    return createNewRegisterAndGet(currentSpace.back());
}
std::pair<const Reference, Register>& World::createNewRegisterAndGet(Reference space) {
    auto spaceIterator = memorySpaces.find(space);
    bool spaceExists = spaceIterator != memorySpaces.end();
    if (not spaceExists) {
        LOG_WARN("creating space %ld because it was requested to create a register", space);
        memorySpaces[space];
    }
    while (isRegisterUsed(lastCreatedRegisterId, space)) {
        lastCreatedRegisterId++;
    }
    auto maxRegisters = 100000000;
    if (lastCreatedRegisterId > maxRegisters) {
        lastCreatedRegisterId = AUTOMATICALLY_CREATED_REGISTERS_START_AT;
        LOG_WARN("used %d registers, will try to reuse the oldest ones again", maxRegisters);
    }
    memorySpaces[space].try_emplace(lastCreatedRegisterId);
    return memorySpaces[space].find(lastCreatedRegisterId).operator*();
}


bool World::isRegisterUsed(Reference id) const {
    return isRegisterUsed(id, currentSpace.back());
}

bool World::isRegisterUsed(Reference id, Reference space) const {
    auto spaceIterator = memorySpaces.find(space);
    bool spaceExists = spaceIterator != memorySpaces.end();
    if (not spaceExists) {
        throw randomize::utils::exception::StackTracedException{
                "space " + std::to_string(space) + " doesn't exist as expected"};
    }
    auto &registers = spaceIterator->second;
    return registers.find(id) != registers.end();
}


Reference World::copyToSpace(Reference reference, Reference fromSpace, Reference toSpace) {
    auto [success, registerToCopy] = getRegisterCopy(reference, fromSpace);
    if (not success) {
        throw randomize::utils::exception::StackTracedException{
                "can't copy non-existent register " + std::to_string(reference)};
    }
    auto &[newReference, newRegister] = createNewRegisterAndGet(toSpace);
    newRegister = registerToCopy;
    return newReference;
}

Reference World::copyProgram(Reference subprogramSpace) {
    auto newProgram = createNewProgram();
    memorySpaces[newProgram] = memorySpaces[subprogramSpace];
    return newProgram;
}

std::pair<bool, Reference> World::copySubprogramDeep(Reference programSpaceToCopy) {
    auto newSpace = createNewSpace();
    memorySpaces[newSpace] = memorySpaces[programSpaceToCopy];
    auto [programExists, program] = getRegisterCopy(PROGRAM_REGISTER, newSpace);
    if (not programExists) {
        return {false, 0};
    }
    for (size_t i = 1; i < program.size(); ++i) {
        auto [sentenceExists, sentence] = getRegisterCopy(program[i], newSpace);
        if (not sentenceExists) {
            return {false, 0};
        }
        if (sentence.size() == 0) {
            LOG_DEBUG("empty sentence on register %ld:%ld", newSpace, program[i]);
            return {false, 0};
        }
        if (sentence.at(0) == static_cast<Reference>(Instruction::CALL_SUBPROGRAM)) {
            auto parameterCount = getParameterCount(Instruction::CALL_SUBPROGRAM);
            if (sentence.size() != parameterCount + 1) {
                LOG_DEBUG("instruction %s on register %ld:%ld needs %ld parameters",
                        to_string(Instruction::CALL_SUBPROGRAM).c_str(), newSpace, program[i], parameterCount);
                logMemory();
                return {false, 0};
            }
            auto referenceOfRegisterWithSubprogramReference = sentence.at(1);
            auto [referenceOfSubprogramReferenceExists, referenceOfSubprogramReference] =
                    getRegisterIterator(referenceOfRegisterWithSubprogramReference, newSpace);
            if (not referenceOfSubprogramReferenceExists) {
                return {false, 0};
            }
            Register &subprogramReference = referenceOfSubprogramReference->second;
            if (subprogramReference.size() != 1) {
                LOG_DEBUG("subprogram reference at %ld:%ld should have only 1 value", newSpace,
                        referenceOfRegisterWithSubprogramReference);
                return {false, 0};
            }
            auto [copied, newSubprogramReference] = copySubprogramDeep(subprogramReference[0]);
            if (not copied) {
                return {false, 0};
            }
            subprogramReference[0] = newSubprogramReference;
        }
    }
    return {true, newSpace};
}

bool World::assignRegisterWithoutOverwriting(Reference result, Register registerToCopy) {
    return assignRegisterWithoutOverwriting(result, std::move(registerToCopy), currentSpace.back());
}

bool World::assignRegisterWithoutOverwriting(Reference result, Register registerToCopy, Reference memorySpace) {
    auto &registers = memorySpaces.at(memorySpace);
    bool overwriting = registers.count(result) > 0;
    if (overwriting) {
        std::stringstream ss;
        randomize::utils::print_stacktrace(ss);
        LOG_DEBUG("overwriting register %ld:%ld is forbidden. %s\nbacktrace: %s", memorySpace, result,
                to_string_memory().c_str(), ss.str().c_str());
        return false;
    }
    registers.emplace(result, std::move(registerToCopy));
    return true;
}

void World::deleteProgram(const long programReference) {
    auto[programExists, programRegister] = getRegisterCopy(PROGRAM_REGISTER, programReference);
    if (not programExists) {
        LOG_DEBUG("can't delete program %ld. It does not exist", programReference);
        return;
    }
    for (size_t i = 1; i < programRegister.size(); ++i) {
        auto sentenceExists = getRegisterPtr(programRegister[i], programReference);
        if (sentenceExists){
            auto &sentence = *sentenceExists;
            if (sentence[0] == static_cast<Reference>(Instruction::CALL_SUBPROGRAM)) {
                auto[sentenceParsed, params] = parseSentence<Instruction::CALL_SUBPROGRAM>(sentence);
                if (sentenceParsed) {
                    auto[success, subprogramSpace] = getRegisterValue(params.subprogramSpace, 0, programReference);
                    deleteProgram(subprogramSpace);
                }
            }
        }
    }
    programs.erase(programReference);
    memorySpaces.erase(programReference);
}

bool World::makeExecutable(Reference programSpace) {
    auto wasAlreadyExecutable = not programs.insert(programSpace).second;
    return wasAlreadyExecutable;
}

bool World::makeNonExecutable(Reference programSpace) {
    auto wasAlreadyExecutable = programs.erase(programSpace) > 0;
    return wasAlreadyExecutable;
}

/**
 * Get the register referenced by the first value of the reference.
 * - check that the reference exists
 * - and that it has at leas 1 value
 * - and that the value is a valid reference
 */
std::pair<bool, Registers::iterator> World::getRegisterIteratorByIndirection(Reference referenceToReference) {
    auto[success, reference] = getRegisterValue(referenceToReference, 0);
    if (not success) {
        return {false, Registers::iterator{nullptr}};
    } else {
        return getRegisterIterator(reference);
    }
}

std::pair<bool, Registers::iterator> World::getRegisterIteratorByIndirection(Reference referenceToReference,
        Reference memorySpace) {
    auto[success, reference] = getRegisterValue(referenceToReference, 0, memorySpace);
    if (not success) {
        return {false, Registers::iterator{nullptr}};
    } else {
        return getRegisterIterator(reference, memorySpace);
    }
}

std::pair<bool, Reference> World::getRegisterValue(Reference reference, long index) const {
    return getRegisterValue(reference, index, currentSpace.back());
}

std::pair<bool, Reference> World::getRegisterValue(Reference reference, long index, Reference memorySpace) const {
    auto aRegister = getRegisterPtr(reference, memorySpace);
    if (not aRegister or index < 0 or aRegister->size() <= static_cast<size_t>(index)) {
        LOG_DEBUG("register %ld:%ld has only %lu values", currentSpace.back(), reference, aRegister->size());
        return {false, {}};
    } else {
        return {true, (*aRegister)[index]};
    }
}

std::pair<bool, Register> World::getRegisterCopy(Reference reference) const {
    return getRegisterCopy(reference, currentSpace.back());
}

std::pair<bool, Register> World::getRegisterCopy(Reference reference, Reference space) const {
    auto spaceIterator = memorySpaces.find(space);
    bool spaceExists = spaceIterator != memorySpaces.end();
    if (not spaceExists) {
        LOG_DEBUG("can't read reference %ld from space %ld: space doesn't exist: %s", reference, space,
                to_string_memory().c_str());
        return {false, Register{}};
    }
    const auto &registers = spaceIterator->second;
    auto registerIterator = registers.find(reference);
    bool registerExists = registerIterator != registers.end();
    if (registerExists) {
        return {true, registerIterator->second};
    } else {
        LOG_DEBUG("can't read reference %ld:%ld it doesn't exist: %s", space, reference, to_string_memory().c_str());
        return {false, Register{}};
    }
}

std::optional<const Register * const> World::getRegisterOptional(Reference reference) const {
    return getRegisterOptional(reference, currentSpace.back());
}
std::optional<const Register * const> World::getRegisterOptional(Reference reference, Reference space) const {
    auto spaceIterator = memorySpaces.find(space);
    bool spaceExists = spaceIterator != memorySpaces.end();
    if (not spaceExists) {
        LOG_DEBUG("can't read reference %ld from space %ld: space doesn't exist: %s", reference, space,
                to_string_memory().c_str());
        return {};
    }
    const auto &registers = spaceIterator->second;
    auto registerIterator = registers.find(reference);
    bool registerExists = registerIterator != registers.end();
    if (registerExists) {
        return {&(registerIterator->second)};
    } else {
        LOG_DEBUG("can't read reference %ld:%ld it doesn't exist: %s", space, reference, to_string_memory().c_str());
        return {};
    }
}

std::pair<bool, Registers::iterator> World::getRegisterIterator(Reference reference) {
    auto registerIterator = getCurrentRegisters().find(reference);
    bool registerExists = registerIterator != getCurrentRegisters().end();
    if (not registerExists) {
        LOG_DEBUG("can't read reference %ld:%ld: it doesn't exist", currentSpace.back(), reference);
        return {false, getCurrentRegisters().end()};
    } else {
        return {true, registerIterator};
    }
}

std::pair<bool, Registers::iterator> World::getRegisterIterator(Reference reference, Reference memorySpace) {
    auto spaceIterator = memorySpaces.find(memorySpace);
    bool spaceExists = spaceIterator != memorySpaces.end();
    if (not spaceExists) {
        LOG_DEBUG("can't read reference %ld from space %ld: space doesn't exist: %s", reference, memorySpace,
                to_string_memory().c_str());
        return {false, Registers::iterator{nullptr}};
    }
    auto &registers = spaceIterator->second;
    auto registerIterator = registers.find(reference);
    bool registerExists = registerIterator != registers.end();
    if (not registerExists) {
        LOG_DEBUG("can't read reference %ld:%ld: it doesn't exist: %s", memorySpace, reference,
                to_string_memory().c_str());
        return {false, Registers::iterator{nullptr}};
    } else {
        return {true, registerIterator};
    }
}

Register const * const World::getRegisterPtr(Reference reference) const {
    return getRegisterPtr(reference, currentSpace.back());
}

Register const * const World::getRegisterPtr(Reference reference, Reference space) const {
    auto spaceIterator = memorySpaces.find(space);
    bool spaceExists = spaceIterator != memorySpaces.end();
    if (not spaceExists) {
        LOG_DEBUG("can't read reference %ld from space %ld: space doesn't exist: %s", reference, space,
                to_string_memory().c_str());
        return nullptr;
    }
    const auto &registers = spaceIterator->second;
    auto registerIterator = registers.find(reference);
    bool registerExists = registerIterator != registers.end();
    if (registerExists) {
        return &(registerIterator->second);
    } else {
        LOG_DEBUG("can't read reference %ld:%ld it doesn't exist: %s", space, reference, to_string_memory().c_str());
        return nullptr;
    }
}
std::string World::to_string_complete_program(Reference program) const {
    const Register &programRegister = memorySpaces.at(program).at(PROGRAM_REGISTER);
    std::stringstream ss;
    ss << "Program summary: " << to_string_sentence(programRegister) << ". Sentences: " << std::endl;

    for (size_t sentenceIndex = 1; sentenceIndex < programRegister.size(); ++sentenceIndex) {
        auto sentenceReference = programRegister[sentenceIndex];
        auto sentence = memorySpaces.at(program).at(sentenceReference);
        ss << to_string_sentence(sentence) << std::endl;
    }
    return ss.str();
}

std::string World::to_string_memory() const {
    std::stringstream ss;
    if (currentInstruction.size() > 0 and currentSpace.size() >= currentInstruction.size()) {
        ss << "\nExecuting " << currentSpace.back() << ":" << currentInstruction.back() << " (instruction at reference "
                << currentInstruction.back() << " of program at space " << currentSpace.back() << ")\n";
        for (long i = currentInstruction.size() - 2; i >= 0; --i) {
            ss << "  above was called from " << currentSpace[currentSpace.size() - (currentInstruction.size() - i)]
                    << ":"
                    << currentInstruction[i] << "\n";
        }
    }
    ss << "\nWorld has " << memorySpaces.size() << " memory spaces:\n";
    for (const auto& [space, registers] : memorySpaces) {
        ss << to_string_memory_space(space, registers);
    }
    return ss.str();
}

std::string World::to_string_memory_space(const Reference space) const {
    return to_string_memory_space(space, memorySpaces.at(space));
}

std::string World::to_string_memory_space(const Reference space, const std::map<Reference, Register> &registers) const {
    std::stringstream ss;
    ss << "  space " << space << ":\n";
    auto fillWidth = log10(registers.size()) + 1;
    for (const auto &[reference, aRegister] : registers) {
        fillWidth = std::max(fillWidth, log10(reference) + 1);
    }
    auto defaultWidth = ss.width();
    for (const auto &[reference, aRegister] : registers) {
        ss << "    reference " << std::setw(fillWidth) << reference << std::setw(defaultWidth) << ": ";
        ss << to_string(aRegister) << "\n";
    }
    return ss.str();
}

void World::logMemory() const {
    LOG_DEBUG("%s", to_string_memory().c_str());
}

bool World::executeSentences(const Reference &reference, const std::vector<Reference> &sentences) {
    for (const auto &sentence : sentences) {
        if (not execute(sentence)) {
            return false;
        }
    }
    return true;
}

/**
 * across the "execute" functions in this file I use the pattern of "if (not success) { return false; }"
 * because this is going to be a common execution path and C++ exceptions are meant for more exceptional cases, and the
 * exceptions would all be handled in the same way: log and continue.
 */
bool World::executeProgramReference(const Reference &reference) {
    auto currentSpaceRaii = AtConstructionAndAtDestruction{
            [&]() { currentSpace.push_back(reference); },
            [&]() { currentSpace.pop_back(); }
    };
    auto program = getRegisterPtr(PROGRAM_REGISTER, reference);
    if (not program) {
        return false;
    }
    return execute(reference, *program);
}

bool World::execute(const Reference &reference) {
    auto aRegister = getRegisterPtr(reference);
    if (not aRegister) {
        return false;
    }
    return execute(reference, *aRegister);
}

template <Instruction MY_INSTRUCTION>
struct ExecutorFunctor {
    bool operator()(World &world, const Reference &reference, const Register &sentence) {
        auto[success, params] = parseSentence<MY_INSTRUCTION>(sentence);
        if (not success) {
            return false;
        }
        return execute(world, reference, params);
    }
};

bool World::execute(const Reference &reference, const Register &sentence) {
    if (sentence.size() == 0) {
        LOG_DEBUG("can not execute empty sentence (%ld)", reference);
        return false;
    }
    auto instruction = static_cast<Instruction>(sentence[0]);
    auto currentSpaceRaii = AtConstructionAndAtDestruction{
            [&]() { if (instruction != Instruction::PROGRAM) { currentInstruction.push_back(reference); } },
            [&]() { if (instruction != Instruction::PROGRAM) { currentInstruction.pop_back(); } }
    };
    return callTemplated<ExecutorFunctor>(instruction, *this, reference, sentence);
}
