/**
 * @file Tools.h
 */

#ifndef GENARCH_TOOLS_H
#define GENARCH_TOOLS_H

#include <functional>
using Func = std::function<long (long)>;

inline long maxWithin(Func function, long leftInput, long rightInput, long leftOutput, long rightOutput) {
    bool recalculateLeft = false;
    bool recalculateRight = false;
    while (leftInput != rightInput) {
        if (recalculateLeft) {
            leftOutput = function(leftInput);
            recalculateLeft = false;
        }
        if (recalculateRight) {
            rightOutput = function(rightInput);
            recalculateRight = false;
        }
        auto pivotInput = (leftInput + rightInput) / 2;
        bool roundingErrors = leftInput < 0 and pivotInput * 2 != (leftInput + rightInput);
        if (roundingErrors) {
            pivotInput--;
        }
        if (leftOutput <= rightOutput) {
            leftInput = pivotInput;
            recalculateLeft = true;
        } else {
            rightInput = pivotInput;
            recalculateRight = true;
        }
    }
    return leftInput;
}

inline long maxWithin(Func function, long leftInput, long rightInput) {
    return maxWithin(function, leftInput, rightInput, function(leftInput), function(rightInput));
}

inline long maximize(Func function) {
    long stepWidth = 64;
    long leftInput = 0;
    long rightInput = leftInput + stepWidth;
    long leftOutput = function(leftInput);
    long rightOutput = function(rightInput);
    long increases = 0;
    if (leftOutput < rightOutput) {
        while (rightOutput > leftOutput) {
            rightInput += stepWidth;
            stepWidth *= 2;
            increases++;
            if (increases > 50) {
                throw randomize::utils::exception::StackTracedException{"passed function doesn't have a clear maximum"};
            }
            rightOutput = function(rightInput);
        }
    } else {
        while (leftOutput >= rightOutput) {
            leftInput -= stepWidth;
            stepWidth *= 2;
            increases++;
            if (increases > 50) {
                throw randomize::utils::exception::StackTracedException{"passed function doesn't have a clear maximum"};
            }
            leftOutput = function(leftInput);
        }
    }
    long max = maxWithin(function, leftInput, rightInput, leftOutput, rightOutput);
    return max;
}


#endif //GENARCH_TOOLS_H
