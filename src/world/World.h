/**
 * @file World.h
 */

#ifndef GENARCH_WORLD_H
#define GENARCH_WORLD_H

#include <genome/Genome.h>
#include <map>
#include <set>
#include <log/log.h>
#include <utils/exception/StackTracedException.h>
#include <problems/Problem.h>
#include <problems/GiveMe100.h>
#include <problems/GiveMe100Twice.h>
#include <memory>
#include <world/World.h>
#include <world/Tools.h>
#include <random>


class World {
public:
    World(std::vector<Register> instructions, std::unique_ptr<Problem> problem);

    void iterate() {
        if (problem->isSolved()) {
            throw randomize::utils::exception::StackTracedException{
                    "Error: trying to iterate to solve a solved problem"};
        }
        auto programsToExecute = programs;
        LOG_DEBUG("%lu programs to execute: %s", programsToExecute.size(),
                randomize::utils::container_to_string_ss(programsToExecute).c_str());
        std::vector<Reference> activeRegisters;
        for ([[maybe_unused]] const auto &[reference, aRegister] : getCurrentRegisters()) {
            activeRegisters.push_back(reference);
        }
        LOG_DEBUG("active registers: %s", randomize::utils::container_to_string_ss(activeRegisters).c_str());

        for (const auto programReference : programsToExecute) {
            executingProgram = programReference;
            if (not executeProgramReference(programReference)) {
                LOG_DEBUG("deleting program %ld due to failed execution. %s", programReference,
                        to_string_complete_program(programReference).c_str());
                deleteProgram(programReference);
            }
            if (problem->isSolved()) {
                LOG_INFO("Stopping iterations: program %ld solved the problem: %s", programReference,
                        to_string_memory_space(programReference).c_str());
                return;
            }
            if (problem->isFinished() and not problem->isSolved()) {
                LOG_DEBUG("deleting program %ld due to failing to solve the problem."
                        //  " %s"
                          , programReference
                        );
                        //, to_string_complete_program(programReference).c_str());
                deleteProgram(programReference);
            }
            LOG_DEBUG("\n%s", to_string_memory().c_str());
        }
    }


    const std::set<Reference> &getPrograms() const {
        return programs;
    }
    const Registers &getCurrentRegisters() const {
        return memorySpaces.at(currentSpace.back());
    }
    const Reference &getCurrentSpace() const {
        return currentSpace.back();
    }
    const Reference &getExecutingProgram() const {
        return executingProgram;
    }
    Problem &getProblem() { return problem.operator*(); }
    std::string to_string_complete_program(Reference program) const;
    std::string to_string_memory() const;
    std::string to_string_memory_space(const Reference space) const;
    std::string to_string_memory_space(const Reference space, const std::map<Reference, Register> &registers) const;
    void logMemory() const;

    inline Registers& getCurrentRegisters() {
        return memorySpaces[currentSpace.back()];
    }
    std::pair<bool, Register> getRegisterCopy(Reference reference) const;
    std::pair<bool, Register> getRegisterCopy(Reference reference, Reference space) const;
    std::optional<const Register * const> getRegisterOptional(Reference reference) const;
    std::optional<const Register * const> getRegisterOptional(Reference reference, Reference space) const;
    std::pair<bool, Reference> getRegisterValue(Reference reference, long index) const;
    std::pair<bool, Reference> getRegisterValue(Reference reference, long index, Reference memorySpace) const;
    Register const * const getRegisterPtr(Reference reference) const;
    Register const * const getRegisterPtr(Reference reference, Reference space) const;
    std::pair<bool, Registers::iterator> getRegisterIterator(Reference reference);
    std::pair<bool, Registers::iterator> getRegisterIterator(Reference reference, Reference memorySpace);
    std::pair<bool, Registers::iterator> getRegisterIteratorByIndirection(Reference referenceToReference);
    std::pair<bool, Registers::iterator> getRegisterIteratorByIndirection(Reference referenceToReference,
            Reference memorySpace);


    const static inline Reference ITERATION_REGISTER = 1;
    const static inline Reference PROGRAM_REGISTER = 0;

    std::map<Reference, Registers> memorySpaces;
protected:
    std::set<Reference> programs;
    Reference executingProgram;
//    Registers registers;
    std::vector<Reference> currentSpace;
    std::vector<Reference> currentInstruction;
    const Reference TOP_LEVEL_SPACE = 0;
    const long AUTOMATICALLY_CREATED_REGISTERS_START_AT = 20;

    std::unique_ptr<Problem> problem;
    size_t maxPrograms = 30000;
    size_t maxSpaces = 30000;
    Reference lastCreatedSpaceId = TOP_LEVEL_SPACE;
    Reference lastCreatedRegisterId = AUTOMATICALLY_CREATED_REGISTERS_START_AT;

public:
    Reference createNewProgram();
//    Reference createNewProgram(Reference space);
    Reference createNewSubprogram();
    Reference createNewSpace();
    Reference createNewRegister();
    Reference createNewRegister(Reference space);
    std::pair<const Reference, Register>& createNewRegisterAndGet();
    std::pair<const Reference, Register>& createNewRegisterAndGet(Reference space);
    bool isRegisterUsed(Reference id) const;
    bool isRegisterUsed(Reference id, Reference space) const;
    Reference copyToSpace(Reference reference, Reference fromSpace, Reference toSpace);
    Reference copyProgram(Reference subprogramSpace);
    std::pair<bool, Reference> copySubprogramDeep(Reference programToCopy);
    bool assignRegisterWithoutOverwriting(Reference result, Register registerToCopy);
    bool assignRegisterWithoutOverwriting(Reference result, Register registerToCopy, Reference memorySpace);
    /**
     * TODO: deep deletion? I see some subprograms left when a toplevel program is deleted.
     * It could even be that the faulty subprogram causes its caller to die, but the faulty subprogram is not deleted.
     */
    void deleteProgram(const long programReference);
    bool makeExecutable(Reference programSpace); ///< @return true if was already executable
    bool makeNonExecutable(Reference programSpace); ///< @return true if was already executable

    bool executeProgramReference(const Reference &reference);
    bool execute(const Reference &reference);
    bool execute(const Reference &reference, const Register &sentence);
    bool executeSentences(const Reference &reference, const std::vector<Reference> &sentences);
};

#endif //GENARCH_WORLD_H
