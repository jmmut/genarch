/**
 * @file BotEntity.cpp
 * @author jmmut
 * @date 2019-03-17.
 */

#include "BotEntity.h"

BotEntity::BotEntity(double maxEnergy, double x, double y, double width, double height)
        : Bot(maxEnergy), LocatedEntity(x, y, width, height) {
}

BotEntity::BotEntity(Bot bot, double x, double y, double width, double height)
        : Bot(bot), LocatedEntity(x, y, width, height) {
}
