/**
 * @file FoodEntity.cpp
 * @author jmmut
 * @date 2019-03-18.
 */

#include <core/utils.h>
#include "FoodEntity.h"

FoodEntity::FoodEntity(double energy, double x, double y, double width, double height)
        : LocatedEntity{x, y, width, height}, maxEnergy(energy), energy(1.0) {
}

double FoodEntity::getEnergy() const {
    return energy * maxEnergy;
}

double FoodEntity::getEnergyRatio() const {
    return energy;
}

void FoodEntity::setEnergy(double energy) {
    FoodEntity::energy = energy / maxEnergy;
}

double FoodEntity::beEaten(double energyEaten) {
    auto previousEnergy = energy;
    energy -= energyEaten / maxEnergy;
    energy = inRange(energy);
    return (previousEnergy - energy) * maxEnergy;
}

bool FoodEntity::isDead() const {
    return energy <= 0;
}
