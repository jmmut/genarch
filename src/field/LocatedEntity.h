/**
 * @file LocatedEntity.h
 * @author jmmut
 * @date 2019-03-17.
 */

#ifndef EMOTIONBOTS_LOCATEDENTITY_H
#define EMOTIONBOTS_LOCATEDENTITY_H

#include <utility>

using Position = std::pair<double, double>;
using Size = std::pair<double, double>;

inline Position operator+(const Position &p1, const Position &p2) {
    return {p1.first + p2.first, p1.second + p2.second};
}

class LocatedEntity {
public:
    LocatedEntity(double x, double y, double width, double height);
    virtual ~LocatedEntity() = default;

    virtual Position getPosition() const;
    virtual void setPosition(Position position);

    virtual Size getSize() const;
    virtual void setSize(Size size);

protected:
    double x, y;
    double width, height;
};


#endif //EMOTIONBOTS_LOCATEDENTITY_H
