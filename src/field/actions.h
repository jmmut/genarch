/**
 * @file actions.h
 * @author jmmut
 * @date 2019-03-21.
 */

#ifndef EMOTIONBOTS_ACTIONS_H
#define EMOTIONBOTS_ACTIONS_H

static const double EATING_SPEED_IN_ENERGY_UNITS_PER_MILLISECOND = 0.03;

#include "field/BotEntity.h"
#include "field/FoodEntity.h"

inline void eat(BotEntity &bot, Edible &food, double milliseconds) {
    double eaten = food.beEaten(EATING_SPEED_IN_ENERGY_UNITS_PER_MILLISECOND * milliseconds);
    bot.eat(eaten);
}

#endif //EMOTIONBOTS_ACTIONS_H
