/**
 * @file Personality.h
 * @author jmmut
 * @date 2019-05-06.
 */

#ifndef EMOTIONBOTS_PERSONALITY_H
#define EMOTIONBOTS_PERSONALITY_H

#include <ostream>
#include <random>

struct Personality {
    double painSensitivity;
    double explorationMotivation;

    Personality mutate() const {
        static std::random_device randomDevice;
        static std::mt19937 randomGenerator{randomDevice()};
        static std::uniform_int_distribution distribution{0, 1};

        auto sample = distribution(randomGenerator);
        auto newPainSensitivity = painSensitivity;
        if (sample == 1) {
            newPainSensitivity *= 1.5;
        } else {
            newPainSensitivity /= 1.5;
        }

        sample = distribution(randomGenerator);
        auto newExplorationMotivation = explorationMotivation;
        if (sample == 1) {
            newExplorationMotivation *= 1.5;
        } else {
            newExplorationMotivation /= 1.5;
        }
        return {newPainSensitivity, newExplorationMotivation};
    }
};

inline std::ostream &operator<<(std::ostream &os, const Personality &personality) {
    os << "painSensitivity: " << personality.painSensitivity
            << " explorationMotivation: " << personality.explorationMotivation;
    return os;
}

#endif //EMOTIONBOTS_PERSONALITY_H
