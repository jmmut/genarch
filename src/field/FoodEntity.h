/**
 * @file FoodEntity.h
 * @author jmmut
 * @date 2019-03-18.
 */

#ifndef EMOTIONBOTS_FOODENTITY_H
#define EMOTIONBOTS_FOODENTITY_H


#include <traits/Edible.h>
#include "LocatedEntity.h"

class FoodEntity : public LocatedEntity, public Edible {
public:
    FoodEntity(double energy, double x, double y, double width, double height);

    double getEnergy() const;
    double getEnergyRatio() const;
    void setEnergy(double energy);
    double beEaten(double energyEaten) override;
    bool isDead () const;

private:
    double maxEnergy;
    double energy;
};


#endif //EMOTIONBOTS_FOODENTITY_H
