/**
 * @file Genome.h
 */

#ifndef GENARCH_GENOME_H
#define GENARCH_GENOME_H

#include <vector>
#include <utils/exception/StackTracedException.h>
#include <utils/Walkers.h>
#include "log/log.h"

using Reference = long;
using Value = Reference;
using MetaReference = Reference ;
using Register = std::vector<Reference>;
using Registers = std::map<Reference, Register>;

/**
 * steps to add another instruction:
 * - add it to this enum
 * - add it to the map of names below
 * - add a struct for its parameters them
 * - add a case entry to the switch in callTemplated
 */
enum class Instruction {
    WHAT_IS_MY_TOPLEVEL_REFERENCE,
    WHAT_IS_MY_REFERENCE,
    COPY_PROGRAM,
    COPY_PROGRAM_DEEP,
    COPY_TO_SPACE,
    REQUEST_INPUT_SIZE,
    MANIPULATE,
    PERCEIVE,
    STORE,
    STORE_IN_SPACE,
    STORE_N_COPIES,
//    ADD,
    CHANGE_VALUE,
    CHANGE_VALUE_OF_PROGRAM,
//    INSERT_VALUE,
//    REMOVE_VALUE,
    GET_RANDOM,
    PROGRAM,
    CALL_SUBPROGRAM,
    CALL_SUBPROGRAM_IF,
    CALL_SUBPROGRAM_ASYNC,
    CALL_SUBPROGRAM_ASYNC_IF,
    CANCEL_CALL_SUBPROGRAM_ASYNC,
    CREATE_SUBPROGRAM_SPACE,
    ADD_TO_SUBPROGRAM,
    DELETE,
    DELETE_PROGRAM,
    MAXIMIZE,
    LOOP,
    LOOP_ASYNC,
    CONCATENATE,
    EQUALS,
    LENGTH,
    NOT,
};

const std::map<Instruction, std::string> instructionNames{
        {Instruction::WHAT_IS_MY_TOPLEVEL_REFERENCE, "WHAT_IS_MY_TOPLEVEL_REFERENCE"},
        {Instruction::WHAT_IS_MY_REFERENCE, "WHAT_IS_MY_REFERENCE"},
        {Instruction::COPY_PROGRAM, "COPY_PROGRAM"},
        {Instruction::COPY_PROGRAM_DEEP, "COPY_PROGRAM_DEEP"},
        {Instruction::COPY_TO_SPACE, "COPY_TO_SPACE"},
        {Instruction::REQUEST_INPUT_SIZE, "REQUEST_INPUT_SIZE"},
        {Instruction::MANIPULATE, "MANIPULATE"},
        {Instruction::PERCEIVE, "PERCEIVE"},
        {Instruction::STORE, "STORE"},
        {Instruction::STORE_IN_SPACE, "STORE_IN_SPACE"},
        {Instruction::STORE_N_COPIES, "STORE_N_COPIES"},
//        {Instruction::ADD, "ADD"},
        {Instruction::CHANGE_VALUE, "CHANGE_VALUE"},
        {Instruction::CHANGE_VALUE_OF_PROGRAM, "CHANGE_VALUE_OF_PROGRAM"},
//        {Instruction::INSERT_VALUE, "INSERT_VALUE"},
//        {Instruction::REMOVE_VALUE, "REMOVE_VALUE"},
        {Instruction::GET_RANDOM, "GET_RANDOM"},
        {Instruction::PROGRAM, "PROGRAM"},
        {Instruction::CALL_SUBPROGRAM, "CALL_SUBPROGRAM"},
        {Instruction::CALL_SUBPROGRAM_IF, "CALL_SUBPROGRAM_IF"},
        {Instruction::CALL_SUBPROGRAM_ASYNC, "CALL_SUBPROGRAM_ASYNC"},
        {Instruction::CALL_SUBPROGRAM_ASYNC_IF, "CALL_SUBPROGRAM_ASYNC_IF"},
        {Instruction::CANCEL_CALL_SUBPROGRAM_ASYNC, "CANCEL_CALL_SUBPROGRAM_ASYNC"},
        {Instruction::CREATE_SUBPROGRAM_SPACE, "CREATE_SUBPROGRAM_SPACE"},
        {Instruction::ADD_TO_SUBPROGRAM, "ADD_TO_SUBPROGRAM"},
        {Instruction::DELETE, "DELETE"},
        {Instruction::DELETE_PROGRAM, "DELETE_PROGRAM"},
        {Instruction::MAXIMIZE, "MAXIMIZE"},
        {Instruction::LOOP, "LOOP"},
        {Instruction::LOOP_ASYNC, "LOOP_ASYNC"},
        {Instruction::CONCATENATE, "CONCATENATE"},
        {Instruction::EQUALS, "EQUALS"},
        {Instruction::LENGTH, "LENGTH"},
        {Instruction::NOT, "NOT"},
};

/**
 * The contents of these parameter structs are usually the sentence registers without the first element, which is the
 * instruction code (enum Instruction).
 */
template<Instruction instruction> struct Parameters {};

template<> struct Parameters<Instruction::WHAT_IS_MY_TOPLEVEL_REFERENCE> {
    Reference result;
};
template<> struct Parameters<Instruction::WHAT_IS_MY_REFERENCE> {
    Reference result;
};
template<> struct Parameters<Instruction::COPY_PROGRAM> {
    Reference result;
    Reference referenceToProgramForCopying;
};
template<> struct Parameters<Instruction::COPY_PROGRAM_DEEP> {
    Reference result;
    Reference referenceToProgramForCopying;
};
template<> struct Parameters<Instruction::COPY_TO_SPACE> {
    Reference result;
    Reference referenceToRegisterToCopy;
    Reference memorySpace;
};
template<> struct Parameters<Instruction::REQUEST_INPUT_SIZE> {
    Reference result;
};
template<> struct Parameters<Instruction::MANIPULATE> {
    Reference action;
};
template<> struct Parameters<Instruction::PERCEIVE> {
    Reference result;
};
template<> struct Parameters<Instruction::STORE> {
    Reference result;
    Value valueToStore;
};
template<> struct Parameters<Instruction::STORE_IN_SPACE> {
    Reference result;
    Value valueToStore;
    Reference memorySpace;
};
template<> struct Parameters<Instruction::STORE_N_COPIES> {
    Reference result;
    Value valueToStore;
    Reference copiesReference;
};
template<> struct Parameters<Instruction::GET_RANDOM> {
    Reference result;
    Reference min;
    Reference max;
};
template<> struct Parameters<Instruction::CHANGE_VALUE> {
    Reference result;
    Reference referenceToTargetReference;
    Reference referenceToTargetValueIndex;
    Reference referenceToNewValue;
};
template<> struct Parameters<Instruction::CHANGE_VALUE_OF_PROGRAM> {
    Reference result;
    Reference referenceToReferenceOfTargetProgram;
    Reference instructionCodeToChange;
    Value valueIndexToChange;
    Reference referenceToNewValue;
};
template<> struct Parameters<Instruction::CREATE_SUBPROGRAM_SPACE> {
    Reference result;
};
template<> struct Parameters<Instruction::PROGRAM> {
    std::vector<Reference> instructions;
};
template<> struct Parameters<Instruction::ADD_TO_SUBPROGRAM> {
    Reference result;
    Register sentence;
};
template<> struct Parameters<Instruction::CALL_SUBPROGRAM> {
    Reference subprogramSpace;
};
template<> struct Parameters<Instruction::CALL_SUBPROGRAM_IF> {
    Reference subprogramSpace;
    Reference jumpIfNonZero;
};
template<> struct Parameters<Instruction::CALL_SUBPROGRAM_ASYNC> {
    Reference wasAlreadyScheduled;
    Reference subprogram;
};
template<> struct Parameters<Instruction::CALL_SUBPROGRAM_ASYNC_IF> {
    Reference wasAlreadyScheduled;
    Reference subprogram;
    Reference jumpIfNonZero;
};
template<> struct Parameters<Instruction::CANCEL_CALL_SUBPROGRAM_ASYNC> {
    Reference wouldHaveExecuted;
    Reference subprogram;
};
template<> struct Parameters<Instruction::DELETE> {
    Reference registerToDelete;
};
template<> struct Parameters<Instruction::DELETE_PROGRAM> {
    MetaReference space;
};
template<> struct Parameters<Instruction::MAXIMIZE> {
    Reference result;
    Reference input;
    Reference output;
    Reference subprogram;
};
template<> struct Parameters<Instruction::LOOP> {
    Reference result;
    Reference subprogram;
    Reference times;
};
template<> struct Parameters<Instruction::LOOP_ASYNC> {
    Reference result;
    Reference subprogram;
    Reference times;
};
template<> struct Parameters<Instruction::CONCATENATE> {
    Reference result;
    Reference toAddAtTheEnd;
};
template<> struct Parameters<Instruction::EQUALS> {
    Reference result;
    Reference a;
    Reference b;
};
template<> struct Parameters<Instruction::LENGTH> {
    Reference result;
    Reference aRegister;
};
template<> struct Parameters<Instruction::NOT> {
    Reference result;
    Reference aRegister;
};


std::string to_string(Instruction instruction);

template<template<Instruction> typename TEMPLATED_FUNCTOR, typename... Args>
auto callTemplated(Instruction instruction, Args&&... args) {
    switch (instruction) {
    case Instruction::PROGRAM:
        return TEMPLATED_FUNCTOR<Instruction::PROGRAM>{}(std::forward<Args>(args)...);
    case Instruction::WHAT_IS_MY_TOPLEVEL_REFERENCE:
        return TEMPLATED_FUNCTOR<Instruction::WHAT_IS_MY_TOPLEVEL_REFERENCE>{}(std::forward<Args>(args)...);
    case Instruction::WHAT_IS_MY_REFERENCE:
        return TEMPLATED_FUNCTOR<Instruction::WHAT_IS_MY_REFERENCE>{}(std::forward<Args>(args)...);
    case Instruction::COPY_PROGRAM:
        return TEMPLATED_FUNCTOR<Instruction::COPY_PROGRAM>{}(std::forward<Args>(args)...);
    case Instruction::COPY_PROGRAM_DEEP:
        return TEMPLATED_FUNCTOR<Instruction::COPY_PROGRAM_DEEP>{}(std::forward<Args>(args)...);
    case Instruction::COPY_TO_SPACE:
        return TEMPLATED_FUNCTOR<Instruction::COPY_TO_SPACE>{}(std::forward<Args>(args)...);
    case Instruction::REQUEST_INPUT_SIZE:
        return TEMPLATED_FUNCTOR<Instruction::REQUEST_INPUT_SIZE>{}(std::forward<Args>(args)...);
    case Instruction::MANIPULATE:
        return TEMPLATED_FUNCTOR<Instruction::MANIPULATE>{}(std::forward<Args>(args)...);
    case Instruction::PERCEIVE:
        return TEMPLATED_FUNCTOR<Instruction::PERCEIVE>{}(std::forward<Args>(args)...);
    case Instruction::STORE:
        return TEMPLATED_FUNCTOR<Instruction::STORE>{}(std::forward<Args>(args)...);
    case Instruction::STORE_IN_SPACE:
        return TEMPLATED_FUNCTOR<Instruction::STORE_IN_SPACE>{}(std::forward<Args>(args)...);
    case Instruction::STORE_N_COPIES:
        return TEMPLATED_FUNCTOR<Instruction::STORE_N_COPIES>{}(std::forward<Args>(args)...);
    case Instruction::GET_RANDOM:
        return TEMPLATED_FUNCTOR<Instruction::GET_RANDOM>{}(std::forward<Args>(args)...);
    case Instruction::CHANGE_VALUE:
        return TEMPLATED_FUNCTOR<Instruction::CHANGE_VALUE>{}(std::forward<Args>(args)...);
    case Instruction::CHANGE_VALUE_OF_PROGRAM:
        return TEMPLATED_FUNCTOR<Instruction::CHANGE_VALUE_OF_PROGRAM>{}(std::forward<Args>(args)...);
    case Instruction::CREATE_SUBPROGRAM_SPACE:
        return TEMPLATED_FUNCTOR<Instruction::CREATE_SUBPROGRAM_SPACE>{}(std::forward<Args>(args)...);
    case Instruction::ADD_TO_SUBPROGRAM:
        return TEMPLATED_FUNCTOR<Instruction::ADD_TO_SUBPROGRAM>{}(std::forward<Args>(args)...);
    case Instruction::CALL_SUBPROGRAM:
        return TEMPLATED_FUNCTOR<Instruction::CALL_SUBPROGRAM>{}(std::forward<Args>(args)...);
    case Instruction::CALL_SUBPROGRAM_IF:
        return TEMPLATED_FUNCTOR<Instruction::CALL_SUBPROGRAM_IF>{}(std::forward<Args>(args)...);
    case Instruction::CALL_SUBPROGRAM_ASYNC:
        return TEMPLATED_FUNCTOR<Instruction::CALL_SUBPROGRAM_ASYNC>{}(std::forward<Args>(args)...);
    case Instruction::CALL_SUBPROGRAM_ASYNC_IF:
        return TEMPLATED_FUNCTOR<Instruction::CALL_SUBPROGRAM_ASYNC_IF>{}(std::forward<Args>(args)...);
    case Instruction::CANCEL_CALL_SUBPROGRAM_ASYNC:
        return TEMPLATED_FUNCTOR<Instruction::CANCEL_CALL_SUBPROGRAM_ASYNC>{}(std::forward<Args>(args)...);
    case Instruction::DELETE:
        return TEMPLATED_FUNCTOR<Instruction::DELETE>{}(std::forward<Args>(args)...);
    case Instruction::DELETE_PROGRAM:
        return TEMPLATED_FUNCTOR<Instruction::DELETE_PROGRAM>{}(std::forward<Args>(args)...);
    case Instruction::MAXIMIZE:
        return TEMPLATED_FUNCTOR<Instruction::MAXIMIZE>{}(std::forward<Args>(args)...);
    case Instruction::LOOP:
        return TEMPLATED_FUNCTOR<Instruction::LOOP>{}(std::forward<Args>(args)...);
    case Instruction::LOOP_ASYNC:
        return TEMPLATED_FUNCTOR<Instruction::LOOP_ASYNC>{}(std::forward<Args>(args)...);
    case Instruction::CONCATENATE:
        return TEMPLATED_FUNCTOR<Instruction::CONCATENATE>{}(std::forward<Args>(args)...);
    case Instruction::EQUALS:
        return TEMPLATED_FUNCTOR<Instruction::EQUALS>{}(std::forward<Args>(args)...);
    case Instruction::LENGTH:
        return TEMPLATED_FUNCTOR<Instruction::LENGTH>{}(std::forward<Args>(args)...);
    case Instruction::NOT:
        return TEMPLATED_FUNCTOR<Instruction::NOT>{}(std::forward<Args>(args)...);
    default:
        std::string instructionName = std::to_string(static_cast<long>(instruction));
        auto message = "can not execute templated function for instruction " + instructionName;
        try {
            message += " " + to_string(instruction);
        } catch (std::exception &e) {
            LOG_DEBUG("%s", message.c_str());
            throw randomize::utils::exception::StackTracedException{message};
        }
        message += ". missing case in switch";
        LOG_DEBUG("%s", message.c_str());
        throw randomize::utils::exception::StackTracedException{message};
    }
}

inline Register makeSentence(Instruction instruction, std::initializer_list<Reference> parameters) {
    Register aRegister;
    aRegister.push_back(static_cast<Reference>(instruction));
    aRegister.insert(aRegister.end(), parameters);
    return aRegister;
}

template<Instruction instruction> Register makeSentence(Parameters<instruction> parameters) {
    Register aRegister;
    aRegister.push_back(static_cast<Reference>(instruction));

    using MyParams = Parameters<instruction>;
    const unsigned long ELEMENT_COUNT = sizeof(MyParams) / sizeof(Reference);
    Reference paramsArray[ELEMENT_COUNT];
    memcpy(&paramsArray, &parameters, sizeof(MyParams));
    for (size_t i = 0; i < ELEMENT_COUNT; ++i) {
        aRegister.push_back(paramsArray[i]);
    }
    return aRegister;
}

template<> inline Register makeSentence(Parameters<Instruction::ADD_TO_SUBPROGRAM> parameters) {
    Register aRegister;
    aRegister.push_back(static_cast<Reference>(Instruction::ADD_TO_SUBPROGRAM));
    aRegister.push_back(parameters.result);
    aRegister.insert(aRegister.end(), parameters.sentence.begin(), parameters.sentence.end());
    return aRegister;
}

inline std::vector<Register> makeSubprogram(Reference subprogramSpace, std::vector<Register> instructions) {
    std::vector<Register> subprogram;
    subprogram.push_back(makeSentence<Instruction::CREATE_SUBPROGRAM_SPACE>({subprogramSpace}));
    for (const auto &instruction : instructions) {
        subprogram.push_back(makeSentence(Parameters<Instruction::ADD_TO_SUBPROGRAM>({subprogramSpace, instruction})));
    }
    return subprogram;
}

inline std::vector<Register> concatenate(std::vector<Register> firstProgramPart,
        std::vector<Register> secondProgramPart) {
    firstProgramPart.insert(firstProgramPart.end(), secondProgramPart.begin(), secondProgramPart.end());
    return firstProgramPart;
}

inline std::vector<Register> concatenate(std::vector<Register> first, std::vector<Register> second,
        std::vector<Register> third) {
    return concatenate(concatenate(first, second), third);
}
inline std::vector<Register> concatenate(std::vector<Register> first, std::vector<Register> second,
        std::vector<Register> third, std::vector<Register> fourth) {
    return concatenate(concatenate(first, second), concatenate(third, fourth));
}

inline std::string to_string(Instruction instruction) {
    auto iterator = instructionNames.find(instruction);
    auto instructionCode = std::to_string(static_cast<long>(instruction));
    if (iterator == instructionNames.end()) {
        throw randomize::utils::exception::StackTracedException{"instruction " + instructionCode + " doesn't exist"};
    } else {
        return instructionCode + ":" + iterator->second;
    }
}

inline std::string to_string_register_data(Register aRegister) {
    return " register: " + randomize::utils::container_to_string_ss(aRegister);
}

inline std::string to_string_sentence(Register sentence) {
    if (sentence.size() == 0) {
        return "empty sentence";
    }
    std::string instructionName;
    try {
        auto instruction = static_cast<Instruction>(sentence[0]);
        instructionName = to_string(instruction);
    } catch (std::exception &exception) {
        instructionName = "(unknown instruction with code " + std::to_string(sentence[0]) + ")";
    }
    if (instructionName == to_string(Instruction::ADD_TO_SUBPROGRAM) and sentence.size() >= 3) {
        return " instruction: " + instructionName + " subprogram:" + std::to_string(sentence[1]) +
                to_string_sentence(std::vector<Reference>{(++ ++sentence.begin()), sentence.end()});
    } else {
        auto restOfSentence = std::vector<Reference>{++sentence.begin(), sentence.end()};
        return " instruction: " + instructionName + " " + randomize::utils::container_to_string_ss(restOfSentence);
    }
}

template <Instruction instruction> unsigned long getParameterCountTemplated() {
    using MyParams = Parameters<instruction>;
    const unsigned long ELEMENT_COUNT = sizeof(MyParams) / sizeof(Reference);
    return ELEMENT_COUNT;
}

template<Instruction I>
struct ParameterCountFunctor {
    auto operator()() {
        return getParameterCountTemplated<I>();
    }
};

inline unsigned long getParameterCount(Instruction instruction) {
    if (instruction == Instruction::PROGRAM or instruction == Instruction::ADD_TO_SUBPROGRAM) {
        return 0;
    } else {
        return callTemplated<ParameterCountFunctor>(instruction);
    }
}

inline std::string to_string(Register aRegister) {
    if (aRegister.size() == 0) {
        return to_string_register_data(aRegister);
    } else {
        try {
            auto instruction = static_cast<Instruction>(aRegister[0]);
            if (instructionNames.count(instruction) != 0) {
                auto count = getParameterCount(instruction);
                if (count == 0 or count + 1 == aRegister.size()) {
                    return to_string_sentence(aRegister);
                }
            }
        } catch (std::exception &exception) {
        }
        return to_string_register_data(aRegister);
    }
}

inline std::ostream &operator<<(std::ostream& out, Register aRegister) {
    return out << to_string(aRegister);
}
inline std::ostream &operator<<(std::ostream& out, Instruction instruction) {
    return out << to_string(instruction);
}

template<Instruction instruction> std::pair<bool, Parameters<instruction>> parseSentence(Register sentence) {
    using MyParams = Parameters<instruction>;
    const unsigned long ELEMENT_COUNT = sizeof(MyParams) / sizeof(Reference);
    if (sentence.size() -1 != ELEMENT_COUNT) { // -1: don't count the first parameter (instruction code)
        LOG_DEBUG("wrong number of parameters (%lu) for instruction %s (expected %lu): %s",
                sentence.size(),
                to_string(static_cast<Instruction>(sentence[0])).c_str(),
                ELEMENT_COUNT + 1,
                to_string(sentence).c_str());
        return {false, {}};
    }
    Reference paramsArray[ELEMENT_COUNT];
    for (size_t i = 0; i < ELEMENT_COUNT; ++i) {
        paramsArray[i] = sentence[i + 1];
    }
    MyParams myParams{};
    memcpy(&myParams, &paramsArray, sizeof(MyParams));
    return {true, myParams};
}

template<> inline std::pair<bool, Parameters<Instruction::ADD_TO_SUBPROGRAM>> parseSentence(Register sentence) {
    size_t expectedCount = 3;
    if (sentence.size() < expectedCount) {
        LOG_DEBUG("wrong number of parameters (%lu) for instruction %s (expected %lu): %s",
                sentence.size(),
                to_string(static_cast<Instruction>(sentence[0])).c_str(),
                expectedCount + 1,
                to_string(sentence).c_str());
        return {false, {}};
    }
    using MyParams = Parameters<Instruction::ADD_TO_SUBPROGRAM>;
    MyParams parameters;
    parameters.result = sentence[1];
    Register subsentence{(++ ++sentence.begin()), sentence.end()};
    parameters.sentence = subsentence;
    return {true, parameters};
}

/**
 * Program sentences are just the instruction code for Instruction::PROGRAM and the list of references of the
 * instructions that make up the program. So just return the sentence without the first element.
 */
template<> inline std::pair<bool, Parameters<Instruction::PROGRAM>> parseSentence(Register sentence) {
    auto instructions = Register{++sentence.begin(),sentence.end()};
    return {true, {instructions}};
}

#endif //GENARCH_WORLD_H
